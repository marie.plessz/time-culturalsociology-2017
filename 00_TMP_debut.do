qui {
	// program: 00_RL_debut.do
	// task: lancer le projet RL Retraite et l�gumes
	// project: RL
	// author: MP le 15/09/2013
	
	*NOTE : si pas sur un ordi o� la macro startd existe, d�finir la macro "projet"
	
	version 12
	clear all
	set linesize 85

	global np "Cultural sociology-2017"

* change path to your project folder in the next line : 
	global projet "$dr/Documents/9-Archive\Travail\2019\TIME\2017_CulturalSociology"

	
	
* next macros define shortcuts to each folder
	global do "$projet/stata/do"
	global source "$projet/stata/data/source" //donn�es originales, ne pas modifier
	global data "$projet/stata/data" //datasets cr��s
	global temp "$projet/stata/data" //datasets temporaires, �tapes, essais etc
	global res "$projet/stata/resultats"
cd "$projet/stata"
}
disp as text "$S_DATE: Bonjour, Stata est pret pour le projet $np"

exit
