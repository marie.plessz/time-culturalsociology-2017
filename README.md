This project contains the stata programs for the following paper: 

Plessz M and Étilé F (2019) Is Cooking Still a Part of Our Eating Practices? Analysing the Decline of a Practice with Time-Use Surveys. _Cultural Sociology_ 13(1): 93–118. DOI: 10.1177/1749975518791431.

Published version on the editor website : https://doi-org.inshs.bib.cnrs.fr/10.1177/1749975518791431.

Or here : https://hal.inrae.fr/hal-01925638 

## Datasets

The datasets can be accessed from their providers for free (praise  for public statistics). The list (note 3, p108 of the paper) includes : 

- Enquête emploi du temps – 1985–1986 (1986) (electronic dataset), INSEE (producer), ADISP – CMH (distributor). Downloaded 14 March 2012.
- Enquête emploi du temps – 2010 (2010) (electronic dataset), INSEE (producer), ADISP – CMH (distributor). Downloaded 14 March 2012.
- (MTUS) Multinational time-use study – 1975 to 2010, version 5.8 (2010) (electronic dataset), Centre for Time-use Research (producer), Centre for Time-use Research (distributor). Downloaded 12 March 2012.
- (ATUS) American time-use study – 2003 to 2010 (2010) (electronic dataset), Bureau of Labour Statistics (producer), Bureau of Labour Statistics (distributor). Downloaded 23 January 2013.
- You will also need the CPS 2010 data because some variables that we use to impute partners' cooking times are in the CPS (Current population survey) files.


Researchers can request free access to the data from their distributors:
- the French surveys here : http://www.progedo-adisp.fr/ 
- MTUS here : https://ukdataservice.ac.uk/find-data/ (new site since our study)
- ATUS here : https://www.bls.gov/tus/ 
- CPS here : https://www.census.gov/programs-surveys/cps/data.html 

## getting started

In order to use the programs : 
- change the path in the dofile  `00_TMP_debut.do` to the root folder on your computer
- place the original US datasets in the `data\source\` folder
- place the original French datasets in folders `data\EDT1988\`, `data\EDT1998\` and `data\EDT2009`
- Running the dofile `TMP_0_mastermaster.do` will launch all the data management steps
- dofiles with `_st_` in their names run statistical analyses : 
    - the main analyses are in `TMP_st_CultSocR1_V3.do`
    - the two other run complementary analyses.
