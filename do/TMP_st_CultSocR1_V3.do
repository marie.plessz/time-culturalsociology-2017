cap log close
set linesize 85
log using "$res/TMP_st_R1CultSoc_V3", text replace

/* R1 : isoler effet de bndom ds les résultats ET traiter bndom comme une var continue... 
toutefois j'ai traité comme continue une var bornée à 4 (150 cas >4 aux USA en 
cumulant les 2 enquetes).
	V2: 18-80 ans, codage continu de bndom jusqu'à 5 en fce et jsq 4 aux USA
	
	V3: stats desc montre que relation fait un plateau après 3 en Fce. je crée bn2 : 
	continu et jusqu'à 4 dans les 2 pays. plus simple à expliquer en plus.
*/

/* /!\ le package oaxaca a été mis à jour. j'utilise ici l'ancienne version.
sur un ordi où mise à jour a été faite (adoupdate) remplacer oaxaca par oaxaca9.
dans ma version l'option pour gérer les vars catégorielles s'appelle categorical
dans la nouvelle elle s'appelle normalize et s'utilise différemment.
*/

* setup
set more off
use  "$data/TMP_3.2_sample", clear
svyset [pw=poidsredressement2]


label def yp 1 "France 1985" 2 "France 2010" 3 "US 1985" 4 "US 2010", modify
label val yp yp

gen condcooking=sumcuisineprinc if somecooking==1

gen parfem=cuisineprinc/sumcuisineprinc*100

recode bndom(5=4 "4+"), gen(bn2)

tab bn2, gen(b)
 
	
******TABLE 1 

/*	Le temps de cuisine a-t-il décliné à l'échelle du ménage?	*/	
******************************************************************
/*Table 1 et 2 condensées en une seule... */

cap erase "$res/R1CultSoc_desc_outreg.txt"
cap erase "$res/R1CultSoc_desc_outreg.csv"
foreach var of varlist sumcuisineprinc  condcooking bn2 bnext {
	quietly svy : regress `var' ibn.yp, nocons	
	outreg2 using "$res/R1CultSoc_desc_outreg",  stats(coef ci) label dec(1) noaster nor2 noobs nodepvar	excel
}	

foreach var of varlist  somecooking b1-b5 couple kids nojob college aged1	aged2 aged3 aged4 aged5 we {
	gen  `var'c= `var'*100
	quietly svy : regress `var'c ibn.yp, nocons	
	drop `var'c
	outreg2 using "$res/R1CultSoc_desc_outreg",  stats(coef ci) label dec(1) noaster nor2 noobs nodepvar	excel
}

foreach var of varlist cuisineprinc parfem {
 quietly svy : regress `var' ibn.yp if sexe==0 & couple==1, nocons
 outreg2 using "$res/R1CultSoc_desc_outreg",  stats(coef ci) label dec(1) ///
	noaster nor2 noobs nodepvar	excel cttop(fem coupl)
 quietly svy : regress `var' ibn.yp if sexe==0, nocons
 outreg2 using "$res/R1CultSoc_desc_outreg",  stats(coef ci) label dec(1) ///
	noaster nor2 noobs nodepvar	excel cttop(fem tout)
 }
*pour les effectifs: 
tab yp
	
***** FIGURE
*association bivariée temps de cuisine nb repas.
/*svy: regress sumcuisineprinc i.bn2#i.enquete#i.country if bn2>0 
margins i.bn2#i.enquete#i.country if bn2>0 
marginsplot , plotdimension(enquete)  ///
	byopt(title("Mean cooking times according to number of eating episodes" "country and year", size(small)) ///
	/*note("Household cooking times and number of eating episodes by main cook, TUS, MP 19 12 2017") */ /// 
	) 	legend ( order(1 "1985" 2 "2010")) ytitle("Minutes (95% CI)", size(small)) ///
		xtitle(Number of eating events/day, size(small)) saving("$res/R1CultSoc_Fig_bivariate1-4", replace) scheme(s1manual )
*/	

svy: regress sumcuisineprinc i.bn2#i.enquete if bn2>0 & country==0
margins i.bn2#i.enquete if bn2>0  & country==0
marginsplot , plotdimension(enquete) title(France)   scheme(s1mono) ///
		legend ( order(3 "1985" 4 "2010" 1 "95% CI" ) col(3) size(vsmall) ) ytitle("Minutes (95% CI)", size(small)) ///
		xtitle(Number of eating events/day, size(small)) saving("$res/cs_fr", replace) ///
		plot1opt(lcolor(black) msymbol(none))  ci1opt(lcolor(black)) ///
		plot2opt(lcolor(black) msymbol(none) lpattern(_))  ci2opt(lcolor(black)) 
		
svy: regress sumcuisineprinc i.bn2#i.enquete if bn2>0 & country==1
margins i.bn2#i.enquete if bn2>0  & country==1
marginsplot , plotdimension(enquete) title(US)   scheme(s1mono) ///
		legend ( order(3 "1985" 4 "2010" 1 "95% CI", textsize(small))) ytitle("Minutes (95% CI)", size(small)) ///
		xtitle(Number of eating events/day, size(small)) saving("$res/cs_us", replace)  ///
		plot1opt(lcolor(black) msymbol(none))  ci1opt(lcolor(black)) ///
		plot2opt(lcolor(black) msymbol(none) lpattern(_))  ci2opt(lcolor(black)) 
grc1leg		"$res/cs_fr" "$res/cs_us",  scheme(s1mono) ycommon saving("$res/R1CultSoc_Fig1_v3", replace)
		
************* TABLE 3
/*	L'association entre nombre de repas et durée de la cuisine a-t-elle changé dans les 2 pays?
*/
* toutes les variables sont codées de façon à faire apparaître ce qui fait "plus cuisiner"

local varexp " bn2  couple kids nojob college 	aged2 aged3 aged4 aged5 we"
cap erase "$res/R1CultSoc_regress.txt"
cap erase "$res/R1CultSoc_regress.csv"
	forvalues ct=1/4	{
	quietly svy:  reg sumcuisineprinc `varexp' if ct==`ct'
	eststo m`ct'
	outreg2 using "$res/R1CultSoc_regress",  stats(coef ci) label dec(1) rdec(2) noaster nodepvar sideway	excel bracket
	}	

*** tests des changements au fil du temps 
* Fce
suest m1 m2, svy	vce(robust) 
test [m1]_b[bn2]=[m2]_b[bn2]
*USA
suest m3 m4, svy vce(robust)
test [m3]_b[bn2]=[m4]_b[bn2]		
	
*** réplication sur les femmes		
cap erase "$res/R1CultSoc_regress_femmes.txt"
cap erase "$res/R1CultSoc_regress_femmes.csv"
forvalues ct=1/4	{
	quietly svy:  reg sumcuisineprinc `varexp' if ct==`ct' & sexe==0
	eststo mck`ct'
	outreg2 using "$res/R1CultSoc_regress_femmes",  stats(coef ci) label dec(1) rdec(2) noaster nodepvar sideway	excel bracket
	}	

cap log close

********** TABLE 4 :  OAXACA *****************

	# delimit ;
local varexp2 " (#dom_meals: bn2)  (controls: single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";	
local cat "aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", replace  noobs bdec(2) ctitle("France") 
	side ci onecol   ;		

		
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", append  noobs bdec(2) ctitle("USA") 
	side ci onecol 	 ;				 
	
********** Appendix C :  OAXACA femmes *****************
* descriptive stats femmes




* oaxaca femmes

	# delimit ;
local varexp2 " (#dom_meals: bn2)  (controls: single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";	
local cat "aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if  country==0  & sexe==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca_femmes", replace  noobs bdec(2) ctitle("France") 
	side ci onecol   ;		

		
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1  & sexe==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca_femmes", append  noobs bdec(2) ctitle("USA") 
	side ci onecol 	 ;				 
	
