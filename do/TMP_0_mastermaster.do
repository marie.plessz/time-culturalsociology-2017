/**********************
Projet	TIME
Auteur	MP
date	06/04/2016
Tache
	master do file
	lance tous les dofiles utiles.
************************/
set more off

*préparer les données de chaque pays
do "$do/TMP_US_0_master"		// préparer data US
do "$do/TMP_Fce_0_master"		// préparer data Fce

* append et recodages sur data fusionnées
do "$do/TMP_1_append_v2"			// sélectionner vars et append les 2 fichiers
do "$do/TMP_2_recode_v2"			// recoder le fichier append
do "$do/TMP_3_select age"			// selectionner échantillon


* analyses
do "$do/TMP_st_CultSocR1_V3.do"
