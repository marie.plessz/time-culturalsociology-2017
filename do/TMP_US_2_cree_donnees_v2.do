
/*****************************
projet : 	TIME
auteur : 	Marie Plessz
date : 		21 juillet 2014
t�che :
	. pr�paration donn�es US
	. repris le 18 juillet pour v�rifier que tout va bien

Le 07/01/2013 : corrig� une erreur dans le label de sitconj
	supprim�  agegr qui doit �tre cr��e plus tard
*/

di  "This program was run at $S_TIME on $S_DATE"

clear
 clear matrix
set more 1
set matsize 800
set mem 500m


     ************
******CARNETS (W6) *****
***************************
        ********
clear
		set mem 1000m		
use "$source/MTUS_W6.DTA", clear

*garder les enqu�tes 1975, 1985 et 2010 des USA
keep if countrya==38
keep if inlist(survey, 1985, 2003)
drop if inrange(year, 2003, 2009) | year==2011
save "$data/MTUS_USA_carnet.DTA", replace
sort hldid persid id



*extraire les temps quotidiens pour les diverses activit�s

quietly {
	local Lrepas " main==5 | sec==5 | main==6 | sec==6 | main==39 | sec==39  "
	local Lrepastra " main==5 | sec==5 |((main== 6 |sec==6) & (eloc==3 | eloc==4)) "
	local Lrepasdom " (main==6 | sec==6 ) & eloc<2 "
	local Lrepasres " main==39 | sec==39 | ((main==6 | sec==6 ) & eloc==6) "
	local Lrepasaut " (main==6|sec==6) & inlist(eloc,2, 5,7,8,9) "
	local Lrepasprinc " main==5 |  main==6 | main==39  "
	local Lrepasprincsssec " (main==5 |  main==6 | main==39) & sec==69 "
	local Lrepasprincsec " (main==5 |  main==6 | main==39) & sec>0 & sec<69 "
	local Lrepasprincvid " (main==5 |  main==6 | main==39) & inlist(sec, 59,60,61) "
	local Lrepasseulprinc " (main==5 |  main==6 | main==39) & alone==1 "
	local Lrepassecseul " (sec==5 |  sec==6 | sec==39) & alone==1 "
	local Lrepassec " sec==5 |  sec==6 |  sec==39  "
	local Lrepassecvid " (sec==5 |  sec==6 |  sec==39 ) & inlist(main, 59,60,61) "
	local Lcuisine " main==18 | sec==18 "
	local Lcuisineprinc " main==18 "
	local Lcuisineprincsec " main==18 &  sec>0 & sec<69 "
	local Lcuisineprincsssec " main==18 & sec==69 "
	local Lcuisineprincvid " main==18  &  inlist(sec, 59,60,61) "
	local Lcuisineprincseul " main==18 & alone==1 "
	local Lcuisinesec " sec==18 "
	local Lcuisinesecvid " sec==18 &   inlist(main, 59,60,61) "
	local Lsport " inlist(main, 42, 43, 44) "
	local Ltravailsal " (main>6 & main<14) | main==63 | (main==5 & eloc==3) "
	local Lrepastraprinc " main==5 | (main== 6 & (eloc==3 | eloc==4)) "
	local Lrepasdomprinc " main==6 & eloc<2 "
	local Lrepasresprinc " main==39 | (main==6 & eloc==6) "
	local Lrepasautprinc " (main==6) & inlist(eloc,2, 5,7,8,9) "
	local Letude " (main>15 & main<17) "
	local Lrepaspartprinc  " (main==5 |  main==6 | main==39) & alone==0 "
	local Lrepasdompartprinc " main==6 & eloc==1 & alone==0 "
	local Lrepastrapartprinc " main==5 | (main==6 & inlist(eloc, 3, 4)) & alone==0 "
	local Lrepasextpartprinc " inlist(main, 6, 39) & eloc!=1 & alone==0 "
	local Lrepasdomseulprinc " main==6 & eloc==1 & alone==1 "
	local Lrepastraseulprinc " main==5 | (main==6 & inlist(eloc, 3, 4)) & alone==1 "
	local Lrepasextseulprinc " inlist(main, 6, 39) & eloc!=1 & alone==1 "
	local Lrepasdomfamprinc " main==6 & eloc==1 & (sppart==1 | child==1) "
	local Lrepastrafamprinc " main==5 | (main==6 & inlist(eloc, 3, 4)) & (sppart==1 | child==1) "
	local Lrepasextfamprinc " inlist(main, 6, 39) & eloc!=1 & (sppart==1 | child==1) "
	local Lrepasfamprinc " inlist(main,5, 6, 39)  & (sppart==1 | child==1) "
	local Lmealchores " main==19 "
	local Lrepasextprinc " inlist(main, 6, 39) & !inlist(eloc, 1, 3, 4) "

	

	local varlist repas repastra repasdom repasres repasaut repasprinc repasprincsssec repasprincsec repasprincvid ///
		repasseulprinc repassecseul repassec repassecvid cuisine cuisineprinc cuisineprincsec cuisineprincsssec  ///
		cuisineprincvid cuisineprincseul cuisinesec cuisinesecvid sport travailsal repastraprinc repasdomprinc ///
		repasresprinc repasautprinc etude repaspartprinc repasdompartprinc repastrapartprinc repasextpartprinc ///
		repasdomseulprinc repastraseulprinc repasextseulprinc repasdomfamprinc repastrafamprinc repasextfamprinc ///
		repasfamprinc mealchores repasextprinc
	
	foreach X in `varlist' { 
		gen a= `L`X''
		by hldid persid id: egen n`X'=sum(a)
		replace a=a * time
		by hldid persid id: egen `X'=sum(a)
		cap drop a
	}
	
	replace repasprincsssec=. if sec<1 | survey==2003
	replace repasprincsec=. if sec<1 | survey==2003
	replace repassecseul=. if sec<1 | survey==2003
	replace repassec=. if sec<1 | survey==2003
	replace repasprincvid=. if sec<1 | survey==2003
	replace repassecvid=. if sec<1 | survey==2003
	replace cuisineprincsssec=. if sec<1 | survey==2003
	replace cuisineprincsec=. if sec<1 | survey==2003
	replace cuisineprincvid=. if sec<1 | survey==2003
	replace cuisinesec=. if sec<1 | survey==2003
	replace cuisinesecvid=. if sec<1 | survey==2003
	replace repasseulprinc=. if alone<0 | survey==1985
	replace cuisineprincseul=. if alone<0 | survey==1985
	replace nrepasprincsssec=. if sec<1 | survey==2003
	replace nrepasprincsec=. if sec<1 | survey==2003
	replace nrepassec=. if sec<1 | survey==2003
	replace nrepasprincvid=. if sec<1 | survey==2003
	replace nrepassecvid=. if sec<1 | survey==2003
	replace ncuisineprincsssec=. if sec<1 | survey==2003
	replace ncuisineprincsec=. if sec<1 | survey==2003
	replace ncuisineprincvid=. if sec<1 | survey==2003
	replace ncuisinesec=. if sec<1 | survey==2003
	replace ncuisinesecvid=. if sec<1 | survey==2003
	replace nrepasseulprinc=. if alone<0 | survey==1985
	replace ncuisineprincseul=. if alone<0 | survey==1985
	replace repas=. if sec<1 |survey==2003
	replace repasdom=. if sec<1 |survey==2003
	replace repastra=. if sec<1 |survey==2003
	replace repasres=. if sec<1 |survey==2003
	replace repasaut=. if sec<1 |survey==2003
	replace cuisine=. if sec<1 |survey==2003
	replace nrepas=. if sec<1 |survey==2003
	replace nrepasdom=. if sec<1 |survey==2003
	replace nrepastra=. if sec<1 |survey==2003
	replace nrepasres=. if sec<1 |survey==2003
	replace nrepasaut=. if sec<1 |survey==2003
	replace ncuisine=. if sec<1 |survey==2003
	replace 	repaspartprinc	=. if survey==1985
	replace 	repasdompartprinc	=. if survey==1985
	replace 	repastrapartprinc	=. if survey==1985
	replace 	repasextpartprinc	=. if survey==1985
	replace 	repasdomseulprinc	=. if survey==1985
	replace 	repastraseulprinc	=. if survey==1985
	replace 	repasextseulprinc	=. if survey==1985
	replace 	nrepaspartprinc	=. if survey==1985
	replace 	nrepasdompartprinc	=. if survey==1985
	replace 	nrepastrapartprinc	=. if survey==1985
	replace 	nrepasextpartprinc	=. if survey==1985
	replace 	nrepasdomseulprinc	=. if survey==1985
	replace 	nrepastraseulprinc	=. if survey==1985
	replace 	nrepasextseulprinc	=. if survey==1985
	replace 	repasfamprinc	=. if survey==1985	
	replace 	repasdomfamprinc	=. if survey==1985	
	replace 	repastrafamprinc	=. if survey==1985	
	replace 	repasextfamprinc	=. if survey==1985	

}

*garder une ligne par carnet
by hldid persid id: gen i=_n
keep if i==1
drop i
save  "$data/MTUS_USA_carnet1li.DTA", replace


       **********************
******FICHIER INDIVIDUS (W58) *****
       ********************
             ********

use "$source/MTUS_W58.DTA", clear
save "$data/MTUS_USA_ind.DTA", replace

**********************************************
*garder les variables et lignes pertinentes
**********************************************

*garder les enqu�tes 1975, 1985 et 2010 des USA
keep if countrya==38
keep if inlist(survey, 1985, 2003)
drop if inrange(year, 2003, 2009) | year==2011
/*
*v�rification des valeurs manquantes selon l'enqu�te
foreach var of varlist day month year badcase hhldsize nchild incorig urban computer  vehicle ///
 sex age famstat singpar civstat empstat student retired workhrs occup sector educa edtry { 
tab `var' survey
}
*/
*garder les var utiles
keep countrya survey hldid persid id day month year swave badcase hhldsize partid relrefp cohab cphome nchild agekid2 agekidx incorig income ///
urban sex age famstat singpar hhtype  civstat empstat student unemp retired workhrs occup sector educa  ///
edtry main63 main5 main6 main39 av1 ocombwt propwt 

gen  double hldid1985=hldid
replace hldid1985=-hldid+(id*0.1) if survey!=1985

gen double persid2003=_n if survey!=2003
replace persid2003=persid if survey==2003

save "$data/MTUS_USA_ind.DTA", replace
*******************************************************
* ajout de var pris dans d'autres fichiers des enqu�tes
*******************************************************

sort survey hldid1985 persid
cap drop _merge
save "$data/MTUS_USA_ind.DTA", replace
merge 1:1 hldid1985 persid using "$data/USA85_ajout.dta"

cap drop _merge
sort survey  persid2003
merge 1:1 persid2003 using  "$data/USA2010_ajout.dta"
cap drop _merge hldid1985 persid2003 
************
*recodages
************

*statut d'emploi : Plein, partiel, �tudiant, chomeur, retrait�, autre sans emploi
gen statemploi=6
replace statemploi=1 if empstat==1
replace statemploi=2 if empstat==2 | empstat==3 //les horaires inconnus ac les temps partiels (28 cas)
replace statemploi=3 if student==1 & emp!=1
replace statemploi=4 if unemp==1 & emp!=1
replace statemploi=5 if retired==1 & emp!=1

label def statemploi 1 "Temps plein" 2 "Temps partiel" 3 "�tudiant" 4 "Ch�meur" 5 "Retrait�" 6 "Autre inactif"
label var statemploi statemploi

gen statemploir=(statemploi<3) //codate synth�tique 1=travaille, 0=travaille pas
label var statemploir "En emploi"

rename statemploisp statemploirsp

g urban2= urban
	replace urban2=. if urban<0
	replace urban2=0 if urban==1
	label def urb 1 "Urbain" 0 "Rural"
	label var urban2 urb

replace revindtrav=revindtrav85 if survey==1985
replace salhor=salhor85 if survey==1985
replace heursem=heursem85 if survey==1985

*salaire horaire en $ 2010, manquant si manquant ou si > p99, nul si out of labour force.
gen wagerate=salhor if survey==2003
replace wagerate=salhor*2.027165572 if survey==1985
/*_pctile a , p(99)
replace a=. if a>r(r1)  // je mets manquants les 2 derniers pctiles. (118 obs)
*/

replace wagerate=0 if empstat==4 | (empstat<4 & wagerate==.) // j'attribue un salaire nul aux pers hors emploi

foreach var of varlist revindtrav  salhor wagerate  heursem {
	replace `var'=0 if statemploir==1 & (`var'==.)
	replace `var'=0 if statemploir==0 & `var'==.
	gen `var'_mis= (`var'==0 & statemploir==1)
}


/*log du sal horaire en $ 2010. 
gen logsalhor2010o2=ln(a)
drop a
*/


gen jour1=swave<2
*jour1 vaut 1 si c'est le seul jour de l'enqu�te ou si c'est un jour de la premi�re vague (enqu�te 1975)

recode sex (2=0), gen(sexe)

*education
gen educ3niv=edtry
recode educ3niv (-8=.) //les missing peuvent aussi �tre regroup�es ac le plus bas niveau
label var educ3niv "Educ<bac; bac; sup"
recode educa (1=1 "�q.coll�ge") (2=2 "�q.lyc�e") (3=3 "�q.bac") (4 5 6= 4 "sup�rieur") (-8=.) , gen(educ)

/*groupes d'�ge
recode age  (0/34=34) (35/59=59)(60/max=60) (-8=.) ,gen(agegr)
*/

*cohorte de naissance
gen an_nais= year-age
gen cohort= trunc(an_nais/10)*10
recode cohort (1890 1900=1910) (1990=1980)
replace cohort=. if age==-8
recode nchild(-8=.), gen(nbenfant)

gen enf4ans=agekidx==1

gen nbadulte=hhldsize-nbenfant
replace nbadulte=. if hhldsize==-8
gen sitconj=civstat
replace sitconj=3 if singpar==1 & civstat==2
recode sitconj (-8=.)
label def sitconj 1"Couple" 2 "Seul" 3 "Monoparental", modify
label val sitconj sitconj


*revenu en continu
gen revenu=.
quietly {
replace revenu= 	1000	 if incorig== 	1	 & survey== 	1975
replace revenu= 	2500	 if incorig== 	2	 & survey== 	1975
replace revenu= 	3500	 if incorig== 	3	 & survey== 	1975
replace revenu= 	4500	 if incorig== 	4	 & survey== 	1975
replace revenu= 	5500	 if incorig== 	5	 & survey== 	1975
replace revenu= 	6750	 if incorig== 	6	 & survey== 	1975
replace revenu= 	8250	 if incorig== 	7	 & survey== 	1975
replace revenu= 	9500	 if incorig== 	8	 & survey== 	1975
replace revenu= 	10500	 if incorig== 	9	 & survey== 	1975
replace revenu= 	11750	 if incorig== 	10	 & survey== 	1975
replace revenu= 	13750	 if incorig== 	11	 & survey== 	1975
replace revenu= 	16000	 if incorig== 	12	 & survey== 	1975
replace revenu= 	18500	 if incorig== 	13	 & survey== 	1975
replace revenu= 	21250	 if incorig== 	14	 & survey== 	1975
replace revenu= 	23750	 if incorig== 	15	 & survey== 	1975
replace revenu= 	27500	 if incorig== 	16	 & survey== 	1975
replace revenu= 	32500	 if incorig== 	17	 & survey== 	1975
replace revenu= 	70000	 if incorig== 	18	 & survey== 	1975
replace revenu= 	7500	 if incorig== 	50	 & survey== 	1985	
replace revenu= 	10000	 if incorig== 	51	 & survey== 	1985	
replace revenu= 	30000	 if incorig== 	52	 & survey== 	1985	
replace revenu= 	70000	 if incorig== 	53	 & survey== 	1985					
replace revenu= 	2500	 if incorig== 	1	 & survey== 	2003	 & year==2010
replace revenu= 	6250	 if incorig== 	2	 & survey== 	2003	 & year==2010
replace revenu= 	8750	 if incorig== 	3	 & survey== 	2003	 & year==2010
replace revenu= 	11250	 if incorig== 	4	 & survey== 	2003	 & year==2010
replace revenu= 	13250	 if incorig== 	5	 & survey== 	2003	 & year==2010
replace revenu= 	17500	 if incorig== 	6	 & survey== 	2003	 & year==2010
replace revenu= 	22500	 if incorig== 	7	 & survey== 	2003	 & year==2010
replace revenu= 	27500	 if incorig== 	8	 & survey== 	2003	 & year==2010
replace revenu= 	32500	 if incorig== 	9	 & survey== 	2003	 & year==2010
replace revenu= 	37500	 if incorig== 	10	 & survey== 	2003	 & year==2010
replace revenu= 	45000	 if incorig== 	11	 & survey== 	2003	 & year==2010
replace revenu= 	55000	 if incorig== 	12	 & survey== 	2003	 & year==2010
replace revenu= 	67500	 if incorig== 	13	 & survey== 	2003	 & year==2010
replace revenu= 	87500	 if incorig== 	14	 & survey== 	2003	 & year==2010
replace revenu= 	125000	 if incorig== 	15	 & survey== 	2003	 & year==2010
replace revenu= 	300000	 if incorig== 	16	 & survey== 	2003	 & year==2010
replace revenu=0 if incorig==-8 | incorig==99
}
la var revenu "revenu du m�nage, 0 si manquant"
*revenu en dollars 2010
gen revenu2010=revenu if survey==2003
replace revenu2010=revenu*2.027165572 if survey==1985
la var revenu2010 "revenu annuel m�nage en dollars2010, 0 si manquant"

gen revenu2010_mis=revenu==0
la var revenu2010_mis "revenu m�nage manquant"

*pour 1985: emploi et revenu du travail du conjoint (qui est aussi dans l'enqu�te)
gen a=revindtrav
replace a=. if revindtrav<0 & survey==1985

quietly {
	forvalues i = 1/7 {
		gen p`i'=(persid==`i') if survey==1985
		gen rpers`i'=a * p`i'
		gen hpers`i'=(a/52)/heursem * p`i'
		gen tpers`i'=heursem*p`i'
		gen apers`i'=age* p`i'
		gen jpers`i'=educa* p`i'
		gen kpers`i'=sex* p`i'
		bys hldid: egen c`i'=sum(rpers`i')
		bys hldid: egen d`i'=sum(hpers`i')
		bys hldid: egen t`i'=sum(tpers`i')
		bys hldid: egen a`i'=sum(apers`i')
		bys hldid: egen j`i'=sum(jpers`i')
		bys hldid: egen k`i'=sum(kpers`i')
		replace c`i'=0 if survey!=1985 | partid != `i'
		replace d`i'=0 if survey!=1985 | partid != `i'
		replace t`i'=0 if survey!=1985 | partid != `i'
		replace a`i'=0 if survey!=1985 | partid != `i'
		replace j`i'=0 if survey!=1985 | partid != `i'
		replace k`i'=0 if survey!=1985 | partid != `i'
		drop p`i' rpers`i' tpers`i' hpers`i' apers`i' jpers`i' kpers`i' 
	} 
}

replace revindtravsp=c1+c2+c3+c4+c5+c6+c7 if survey==1985
replace revindtravsp=. if partid<1 & survey==1985
replace salhorsp= d1+ d2+ d3+ d4+ d5+ d6+ d7 if survey==1985
replace salhorsp=. if partid<1 & survey==1985
replace heursemsp= t1+t2+t3+t4+t5+t6+t7 if survey==1985
replace heursemsp=. if partid<1 & survey==1985
replace agesp=a1+a2+a3+a4+a5+a6+a7 if survey==1985
replace agesp=. if partid<1 & survey==1985
gen educasp=j1+j2+j3+j4+j5+j6+j7 if survey==1985
replace educasp=. if partid<1 & survey==1985

gen sexesp=k1+k2+k3+k4+k5+k6+k7 if survey==1985
replace sexesp=. if partid<1 & survey==1985
replace sexesp=. if sexesp==0
replace sexesp=0 if sexesp==2
cap drop c1-c7 d1-d7 t1-t7 a1-a7 j1-j7 k1-k7

forvalues i = 1/7 {
	gen p`i'=(persid==`i') if survey==1985
	gen epers`i'=statemploir * p`i'
	bys hldid: egen e`i'=sum(epers`i')
	replace e`i'=0 if survey!=1985 | partid != `i'
	drop p`i' epers`i'
} 

replace statemploirsp=e1+ e2+ e3+ e4+ e5+ e6+ e7 if survey==1985
replace statemploirsp =. if partid<1
cap drop e1-e7

replace revindtravsp=. if sitconj!=1
replace salhorsp=. if sitconj!=1
replace heursemsp=. if sitconj!=1

gen wageratesp=salhorsp if survey==2003
replace wageratesp=salhorsp*2.027165572 if survey==1985

foreach var of varlist  revindtravsp  salhorsp wageratesp heursemsp  {
	replace `var'=0 if statemploirsp==1 & (`var'==.)
	replace `var'=0 if statemploirsp==0 & `var'==.
	gen `var'_mis= (`var'==0 & statemploirsp==1)
}


*niveau d'�tude du cjt en 4 modalit�s
cap drop a
gen a=educasp
recode a (1=1 ) (2=2 ) (3=3 ) (4 5 6= 4 ) (-8 0=.)
replace educsp=a if survey==1985
drop a 
*niveau d'�tude d�taill�
replace educasp=educspdet if survey == 2003


/*log du sal horaire du conjoint en $ 2010
cap drop a 
gen a=salhorsp if survey==2003
replace a=salhorsp*2.027165572 if survey==1985
_pctile a , p(99)
replace a=. if a>r(r1)  // je mets manquants le  dernier pctile. 44 obs)
gen salhorsp2010=a
replace salhorsp2010=0 if statemploisp==0 // j'attribue un salaire nul aux pers hors emploi

gen logsalhorsp2010=ln(a)
drop a
*/



*nottsandardhhold 
cap drop a
gen a=(famstat ==(-8) | civstat==-8 ) // les m�nages pour lesquels MTUS n'a pas r�ussi � reconstituer la struct du m�nage
replace a=1 if relrefp>2 //les m�nages dans lesquels il y a un adulte et un ou ses parents
replace notstandardhhold=a //if survey==1985
drop a
*autre version en s'appuyant sur la taille du m�nage, le nombre de membres majeurs et la situation conjugale.
gen y=hhldsize-nchild
gen atypique=0
replace atypique=1 if y>2
replace atypique=1 if sitconj==. | famstat==-8 | civstat==-8 
replace atypique=1 if (sitconj==2 | sitconj==3) & y!=1
replace atypique=1 if sitconj==1 & y!=2
replace atypique=1 if relrefp>2
drop y

*m�nages standards d'�ge actif
g standardhhold1=(notstandardhhold==0 & (age>=18 & age<=64))
g standardhhold2=(notstandardhhold==0 & (age>=18 & age<=59) /*& (agesp>=18 & agesp<=59) */)

la var standardhhold1 "Menage standard parent/enfant avec pr entre 18 et 64 ans"
la var standardhhold2 "Menage standard parent/enfant avec conjoints entre 18 et 60 ans"

compress
sort hldid persid id
save "$data/MTUS_USA_ind.DTA", replace



*merge les fichiers individu et carnet
merge 1:1 hldid persid id using "$data/MTUS_USA_carnet1li.DTA"
drop if _merge==2 | survey==.

drop  _merge time country    clockst  start    end      epnum    main     sec      av       inout    eloc   ///
  ict      mtrav    alone    child    sppart   oad
quietly compress

save  "$data/MTUS_USA_janv2013.DTA", replace
