/********************
Projet : 	Time
auteur : 	Marie
date : 		21juillet2014

tâche: 
	. ajouter des var récupérées dans les fichiers originaux pour USA 1985
	. ajouter des var récupérées dans les fichiers originaux pour USA 2010$
	. macros des chemins changés le 21 juillet 2014
*********************/


*****ajouter des var récupérées dans les fichiers originaux pour USA 1985

*ajoute des variables socio démo présentes dans les données "heritage" mais pas dans mtus


*récupérer les variables utiles plus qqunes pour vérif, renommer celles qu'il faut
use "$source/USA85quest.dta", clear
keep survey wave hhid pid  wagelm wkhrs
gen revindtrav85=wagelm*12
label var revindtrav85 "revenu individuel annuel du travail"
replace revindtrav85=. if wagelm== -8
replace revindtrav85=0 if wagelm== -7
recode wkhrs (-8 -9=.)(-7=0), gen(heursem85)
gen salhor85=(revindtrav/52)/heursem
label var salhor85 "Salaire horaire"
label var heursem85 "heures de travail hebdo habituel"

rename hhid hldid
rename pid persid
gen hldid1985=hldid
sort survey wave hldid1985 persid

save "$data/USA85_ajout.dta", replace


****IDEM pour 2010*********
*regrouper toutes les var à ajouter
use "$source/USA2010_atussum.dta", clear
keep tucaseid teage  trernwa trspftpt tehruslt 
save "$data/USA2010_ajout.dta", replace

merge 1:m  tucaseid using "$data/atuscps_2010_ajout.dta"
cap drop _merge
merge 1:1 tucaseid tulineno using "$source/USA2010_atusrost.dta"
cap drop _merge
merge 1:1 tucaseid tulineno using "$source/USA2010_atusresp.dta"
cap drop _merge

save "$data/USA2010_ajout.dta", replace

*recode
format tucaseid %14.0f

/*ménages non standard : dont les membres ne sont pas égo, son éventuel conjoint, ses éventuels enfants) */
gen a=(terrp<0 | terrp>22) & !inlist(terrp, 27, 40)
bys tucaseid: egen b=sum(a)
gen notstandardhhold=b>0

/* compte nombre d'enfants des différents âges */
gen e1a=prtage<=1
gen e2a=prtage==2
gen e3a=prtage==3
gen e4a=prtage==4
gen e6am=prtage<6
foreach var of varlist e1a-e6am {
	bys tucaseid: egen nb`var'=sum(`var')
}	

drop a b e1a-e6am

recode ptdtrace(1=1) (-3/-1=.)(else=0)    ,gen(white)
recode pehspnon(1=1)(2=0), gen(hispanic)
*recode gtmetsta (3=.), gen(urban)
recode tespempnot (1=1)(2=0) (-1 =.) ,gen(statemploisp)
label define statemploisp -1 "NR" 0 "cjt hors emploi" 1 "cjt en emploi"
label value statemploisp statemploisp

gen revindtrav= 52*trernwa/100
replace revindtrav=. if revindtrav<0

gen heursem=tehruslt
replace heursem=. if tehruslt<0

gen salhor= (trernwa/heursem)/100
replace salhor=. if trernwa==-1


*récupérer variables du cps
gen a=pespouse*tratusr
replace a=0 if pespouse==-1
bys tucaseid: egen a2=sum(a)
gen b=(pulineno==a2)

gen c=prernwa*b
bys tucaseid: egen d=sum(c)
gen revindtravsp=52*d/100
replace revindtravsp=. if revindtravsp<0
label var revindtravsp "Revenu individuel annuel travail conjoint"

gen e=pehruslt*b
replace e=. if pehruslt<0
bys tucaseid: egen f=sum(e)
gen heursemsp= f
replace heursemsp=. if heursemsp<0
label var heursemsp "heures travail hebdo habituel conjoint"

drop e f
gen e=prtage*b
replace e=. if prtage<0
bys tucaseid: egen f=sum(e)
gen agesp= f
replace agesp=. if agesp<1
label var agesp "Age of the spouse"

gen salhorsp= (d/f)/100
replace salhorsp=. if revindtravsp==.
label var salhorsp "Salaire horaire conjoint"

gen g=ptdtrace*b
replace g=. if ptdtrace<0
bys tucaseid: egen h=sum(g)
gen whitesp=h
replace whitesp=. if whitesp<1
replace whitesp=0 if whitesp>1 & whitesp<.
label var whitesp "Race of spouse white"
drop g h

gen g=pehspnon*b
replace g=. if pehspnon<0
bys tucaseid: egen h=sum(g)
gen hispanicsp=h
replace hispanicsp=. if hispanicsp<1
recode hispanicsp (2=0)
label var hispanicsp "Spouse hispanic"
drop g h

gen g=peeduca*b
replace g=. if peeduca<0
bys tucaseid: egen h=sum(g)
recode h (31/34=1 "some highschool") (35/38=2 "éq.lycée") (39=3 "éq.bac") (40/46= 4 "supérieur"), gen (educsp) 
replace educsp=. if educsp<1
label var educsp "Education level of spouse"
rename h educspdet
recode educspdet (31/34=1 ) (35/38=2 ) (39=3 ) (40=4) (41/43=5) (44 45 46=6)
label var educspdet "spouse education 2010 comme educa 4 sommecollege 5 licence 6 master et+"
drop g  

drop a-d e f 
cap drop _merge
*clist tucaseid tratusr pespouse pulineno a a2 b c d prernwa trspftpt agesp teage in 1/30
save "$data/USA2010_ajout.dta", replace
keep if tratusr==1
clist tucaseid tulineno pulineno teage in 1/25
rename tucaseid persid
gen double persid2003=persid
sort persid2003

local varkeep " persid notstandardhhold nbe1a  nbe2a nbe3a nbe4a nbe6am white hispanic  heursem "
local varkeep " `varkeep' revindtrav salhor revindtravsp salhorsp statemploisp "
local varkeep " `varkeep'  heursemsp agesp persid2003 gereg gestcen agesp whitesp hispanicsp educsp educspdet"
di "`varkeep'"
keep  `varkeep'

save "$data/USA2010_ajout.dta", replace
