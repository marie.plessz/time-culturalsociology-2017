/************************************
Projet : 	Time
auteur : 	MP
data :		21 juillet 2014
Tache: 
	. pr�parer variable et s�lection individus pour d�composition
	. corrige les manquantes dans age et catage
*************************************/



set more off
*****************************************
**	Pr�paration pour la d�composition	*
*****************************************


use "$data/MTUS_USA_impute_juin2013.DTA", clear



g sample1=(atypique==0 & age>18 & age<64)	/* Menages standards */
g sample2=(sample1==1 & sexe==1 & statemploi!=3 & statemploi!=5 & statemploi!=6) /* Menages standards avec hommes actifs */
*g sample3=(sample1==1 & sexe==1 & statemploi!=3 & statemploi!=5 & statemploi!=6 & salarie==1) /* Menages standards avec hommes actifs et travail salarie */
/*pas d'info sur statut salari� aux usa */

/* 	recode	comme fabrice	*/
/*controls	*/
replace age=. if age==-8
g age10=age/10
g diffage10=age10-page/10
g age102=age10^2
g age103=age10^3

local varcont "diffage10"

/*je ne peux pas cr�er struct1 etc pour la structure familiale donc je donne des noms diff�rents */
	 g struct5=enf4ans>0  //au moins 1 enf de 0-4 ans
	 g struct6=enf4ans==0 & nbenfant>0 //au moins un enfant aucun de moins de 5 ans
	 g struct7=(nbenfant==0) // pas d'enfants

tabulate gereg, nofreq g(regionus)
local varcont "`varcont' struct5 struct6 urban2 regionus2 regionus3 regionus4 " 

/*	r.h.s. variables	*/
g catage=1 if age<25
forv i=2/12	{
	qui replace catage=`i' if age>=25+5*(`i'-2) & age<25+5*(`i'-1)
				}
replace catage=13 if age>=80 & age<.
replace catage=. if age==.

rename educa educaold
rename peduca peducaold

recode educaold (1 2 =1 "Some highschool") (3=2 "Hs graduate") (4=3 "Some college") (5 6=4 "College")(-8 0=.)   , gen(educa)	
recode peducaold (1 2 =1 "Some highschool") (3=2 "Hs graduate") (4=3 "Some college") (5 6=4 "College") (0=9)(-8 =.)   , gen(peduca)	
gen  peduca1_mis=(peduca>4 & sitconj==1)

rename statemploir worker
rename pstatemploir pworker

g sexhholdseul=sexe*(sitconj!=1)

foreach var in educa peduca  sitconj catage   {
		qui tabulate `var', nofreq g(`var')																				
	}
local varexp2 "catage2 catage3  catage4 catage5  catage6  catage7 catage8  catage9"
local varexp2 "`varexp2' educa1    educa2    educa3 worker sitconj2 sitconj3 sexhholdseul"
local varexp2 "`varexp2' peduca1    peduca2    peduca3 pworker `varcont'"
				
di "`varexp2'"

gen enquete=survey==1985        



/*	Temps de cuisine non-nul	*/
gen somecooking=sumcuisineprinc>0	
replace somecooking=. if sumcuisineprinc==.	
label var somecooking "Any cooking (HH)?"	
label def somecooking 0 "No cooking in HH" 1 "Some cooking in HH"	, modify
label val somecooking somecooking	

/* clean the sample and compute the relative sampleweights */


# delimit ;

/*drop same sex couples and couples where no info on spouse*/
ta enquete sexe;
drop if sexe==psexe & sitconj==1;
drop if sitconj==1 & sexe==1;

/* keep sample1 menage standard age actif */
ta enquete sexe;
keep if sample1==1;
ta enquete sexe;
g sample1a=(sample1==1 & day!=6 & day!=7);

g sample1b=(sample1==1 & (day==6|day==7));
g sample2a=(sample2==1 & day!=6 & day!=7);
g sample2b=(sample2==1 & (day==6|day==7));
*g sample3a=(sample3==1 & day!=6 & day!=7);
*g sample4b=(sample3==1 & (day==6|day==7));


ta enquete sexe;
local vardep "repasprinc";
local allvardep "repasprinc cuisineprinc";/**/

	foreach var of local allvardep	{;
		drop if `var'==.;
								};
ta enquete sexe;
# delimit ;
local varexp "wagerate wagerate_mis pwagerate pwagerate_mis  
				worker pworker revenu2010 revenu2010_mis educa1 educa2 educa3 
				peduca1 peduca2 peduca3  age10 age102 age103 diffage10 nbenfant enf4ans 
			 urban2 sitconj2 sitconj3 "; 
			 /*regionus1-regionus3 */


unab varexp: `varexp';

bys enquete: su `varexp';

	di "`varexp'";		
			
	foreach var of local varexp	{;
		drop if `var'==.;
		ta enquete sexe;
								};

bys sexe enquete: ta sample1a;
ta sexe;
g missing1=(wagerate_mis==1|revenu2010==0|educa==.);
g missing2=(missing1==1|pwagerate_mis==1|peduca==.);
		
		
/* calcul des poids de redressement a partir  de propwt */
*si traitements s�par�s selon sexe ;
qui g double poidsredressement1=. ;
forv enq=0/1	{ ;
	forv s=0/1		{ ;
			qui su propwt if sexe==`s' & enquete==`enq' ;
			qui replace poidsredressement1=propwt/(r(N)*r(mean)) if sexe==`s' & enquete==`enq' ;
					} ;
				} ; 
	sum poidsredressement1;		
	label var poidsredressement1 "Poids par sexe" ;
*si traitement sans distinction de sexe ;
qui g double poidsredressement2=. ;
forv enq=0/1	{ ;
	
			qui su propwt if  enquete==`enq' ;
			qui replace poidsredressement2=propwt/(r(N)*r(mean)) if enquete==`enq' ;
				
				} ; 
ta enquete sexe	[aw=poidsredressement2];
label var poidsredressement2 "Poids par m�nage" ;
recode enquete 1=1985 0=2009;

 # delimit ;
 g old1985=(enquete==1985);

	 
	/*********************************************************************
	***	Recodages adapt�s aux faibles effectifs de l'enqu�te USA1985
	*********************************************************************/

	*Structure des m�nages	;
			/*pour les stats desc	*/
gen pstatus=0	;
replace pstatus=1 if pworker==0 & sitconj==1	;
replace pstatus=2 if pworker==1		;
tab pstatus	;
label var pstatus "Partner status"	;
label def pstatus 0"No partner" 1 "Partner no job" 2 "Partner works"	;
label val pstatus pstatus		;
		/* pour les mod�les	*/
	gen single=	sitconj!=1	;
	label var single "Single-adult HH"	;
	label def single 0 "Couple" 1 "Single", modify	;
	label val single single	;

	gen kids=	nbenfant !=0	;
	label var kids "Children in HH"	;
	label def kids 0 "No kids" 1 "1+ kid in HH", modify	;
	label val kids kids	;
	/*	nokids
gen nokids=1-kids	;
label var nokids "No kids in HH?"	;
label def nokids 0 "Kids" 1 "No kids", modify	;
label val nokids nokids	;
*/
	* niveau d'�tude dichotomique: college ou pas	;
	gen college= 	educa==3| educa==4	;
	gen pcollege=	peduca==3| peduca==4	;
	label def college 0 "No college" 1 "College", modify	;
	label def pcollege 0 "No partner/partner no college" 1 "Partner: college", modify	;
	label val college college	;
	label val pcollege pcollege	;
	label var college "Main Cook college"	;
	label var pcollege "Partner education"	;

	*age :tranches de 10 ans	;
	recode age (min/29= 20) (30/39=30)(40/49=40)(50/64=50)		, gen(aged)	;
	label var aged "Age class"	;
	tab aged, gen(aged)	;
	format aged* %2.0f	;
	 
	*	ou groupe d'age	;

	recode age  (0/34=34) (35/59=59)(60/max=60) (-8=.) ,gen(agegr)	;
	 label def agegr 34 "18-34y" 59 "35-49y" 60 "50-64y", modify	;
	 label val agegr agegr	;
	 label var agegr "Age group"	;
	 format agegr %2.0f	;
	 tab agegr, gen(agegr)	;
 
  *	cohorte de naissance ;
replace an_nais=year-age ;
label var an_nais "Year born Main Cook" ;
recode an_nais (min/1945=1945)(1946/1960=1960)(1961/1975=1975)(1976/max=1990), gen(cohortr)	;
label var cohortr "birth cohort Main Ck" ;
tab cohortr, gen(cohortr) ;

recode an_nais (min/1955= 0) (1956/max=1), gen(bornlt55)	;
label var bornlt55 "Born later than 1955?"	;
label def bornlt55 0 "Born until 1955" 1 "Born after 55";
label val bornlt55 bornlt55	;

	* nombres repas � domicile ;
recode nrepasdomprinc (4 5 6=4 "4 and more"), gen(nhomemeals) ;
label var nhomemeals "N of meals at home (Main Ck)" ;

tab nhomemeals, gen(homem ) ;
rename (homem*) (homem0 homem1 homem2 homem3 homem4) ;

 * labels manquants ;
 label def worker 0 "Main Ck no job" 1 "Main Ck works";
 label val worker worker;
 label var worker "Main cook works?";
 
 label var urban2 "Residential area" ;
 label def urban2 0 "Not metropolitan" 2 "Metropolitan" ;
 label val urban2 urban2 ;
 
 label var sumcuisineprinc "HH total cooking time" ;

 label def we 0 "Weekday" 1 "Week-end";
 label val we we;
 label var we "Week-end?";
 *du nettoyage	;
 
drop   nrepassecseul repassec nrepassec repassecvid nrepassecvid repastraprinc 
	nrepastraprinc  repasresprinc nrepasresprinc repasautprinc
	nrepasautprinc repaspartprinc nrepaspartprinc repasdompartprinc nrepasdompartprinc
	repastrapartprinc nrepastrapartprinc repasextpartprinc nrepasextpartprinc 
	repasdomseulprinc nrepasdomseulprinc repastraseulprinc nrepastraseulprinc repasextseulprinc
	nrepasextseulprinc repasdomfamprinc nrepasdomfamprinc repastrafamprinc nrepastrafamprinc
	repasextfamprinc nrepasextfamprinc repasfamprinc nrepasfamprinc 
	repastra nrepastra repasdom nrepasdom repasres nrepasres repasaut nrepasaut 
	repasprincsssec nrepasprincsssec repasprincsec nrepasprincsec repasprincvid 
	nrepasprincvid repasseulprinc nrepasseulprinc repassecseul
	cuisineprincsec ncuisineprincsec cuisineprincsssec ncuisineprincsssec cuisineprincvid
	ncuisineprincvid cuisineprincseul ncuisineprincseul cuisinesec ncuisinesec 
	cuisinesecvid ncuisinesecvid 
	;

compress;
save "$data/USAestsample_hld.dta", replace ;
