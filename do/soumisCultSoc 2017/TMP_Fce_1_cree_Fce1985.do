 *	pr�paration des donn�es de 1988 Emploi du temps france
*projet TIME, Fabrice Etil�
*revu par Marie le 18juillet 2014

version 11.0
set more off
# delimit;
*global path "C:\DATA\TIME\EDT1988\";
global path "$data\EDT1988\";
*global path "/Users/fabriceetile/Documents/Fabrice - Work/DATA/TIME/EDT1988/";
cd "$path";

cap log cl;
set logtype t;
log using makedataEDT1985, replace;
di in b "This program was run at $S_TIME on $S_DATE";

clear;
clear matrix;
set more 1;
set matsize 800;
set mem 500m;

/* creation d'une variable indiquant si l'individu est dans un menage non standard, 
 cad peut �tre un adulte autre que celui s'occupant des enfants (pere, mere, tuteur principal) etc */

use "${path}individu.dta", clear;

destring _all, replace;
g travail=(occ==10); /* nb: definition beaucuop plus stricte qu'en 2010 puisque bas� sur occupation principale uniquement */
rename pondkc pond_carnet;
rename nquest hldid;
gen double persid=hldid*100+ni03;
gen e1a=age<=1;
gen e2a=age==2;
gen e3a=age==3;
gen e4a=age==4;
gen e6am=age<6;
foreach var of varlist e1a-e6am {;
	bys hldid: egen nb`var'=sum(`var');
}	;
sort hldid;
gen a= (lien>2);
bys hldid: egen b=sum(a);
gen notstandardhhold=b>0;
drop a b;
bys hldid: egen nbtravailleur=sum(travail);
bys hldid: keep if _n==1;
local varkeep "hldid notstandardhhold nbe1a nbe2a nbe3a nbe4a nbe6am nbtravailleur pond_carnet";
keep `varkeep';
sort hldid ;
sa  "${path}temp.dta", replace;


/* recuperation des variables uniquement presentes dans la base menage */

use "${path}mena.dta", clear;
*keep nquest tu nbenf nbap04 nbap02 pondn nb03 promer promerc mena* aida* aidb* aidc* aidd* zeat; 

destring _all, replace;
rename pondn propwtm;
rename pondk pond_kish;
rename nquest hldid;
sort hldid;
merge 1:1 hldid using "${path}temp.dta";
keep if _merge==3;
cap drop _merge;
bys hldid: keep if _n==1;
rename tu tuu;
g rural=(tuu==0);
g urbain1=(tuu==1|tuu==2|tuu==3);
g urbain2=(tuu==4|tuu==5);
g urbain3=(tuu==6|tuu==7);
g paris=(tuu==8);

g microonde=0;
g lavevaisselle=1 if (nbap04>0 & nbap04!=9);
replace lavevaisselle=0 if nbap04==0;
g congel=1 if (nbap02>0 & nbap02!=9)|(nbap03>0 & nbap03!=9);
replace congel=0 if nbap02==0;

local varkeep "`varkeep' tuu rural urbain1 urbain2 urbain3 paris microonde zeat
				lavevaisselle congel propwtm pond_kish";
sort hldid;
sa  "${path}temp.dta", replace;


/* Caracteristique de la Journee enquetee */
/* recuperation des infos sur le jour echantillone et savoir si exceptionnel ou non */

use "${path}kishcj.dta", clear;

keep nquest nib2 joursmb2 jourb2 moisb2 anb2 typjb2 autypb2 excepb2 excautb2 actb2;

destring _all, replace;
rename nquest hldid;
gen double persid=hldid*100+nib2;
sort hldid persid;
merge m:1 hldid using "${path}temp";
keep if _merge==3;
cap drop _merge;


rename joursmb2 day;
rename moisb2 month;
rename anb2 year;
sort hldid persid;

g abnormal=inlist(excepb2, 1 ,2 ,4) | inrange(excautb2, 1, 8) ;
g regularworkday=0 if actb2==1;
replace regularworkday=(actb2==1 & typjb2==1);
g regulardayoff=0 if actb2==1;
replace regulardayoff=(actb2==1 & (typjb2==3|typjb2==6));
la var abnormal "abnormal day";
la var regularworkday "Regular work day for workers";
la var regulardayoff "Day off for workers";
drop actb2 typjb2 excepb2 ;

local varkeep "`varkeep' persid abnormal regulardayoff regularworkday day month year";
sort hldid;
sa  "${path}temp.dta", replace;

/* recuperation des variables individuelles */

use "${path}individu.dta", clear;

keep ikish pondkc dipro dipgen nquest sexe age ni03 cohab occ heurdom heurex mindom minex lien class ancess pcs statut;

destring _all, replace;
rename nquest hldid;
g double persid=hldid*100+ni03;
sort hldid persid;
merge 1:1 hldid persid using "${path}temp";
keep if _merge==3;
cap drop _merge;

/* Echantillon */

g propwti=pond_carnet;
replace propwti=pond_kish if propwti==0;
g standardhhold1=(notstandardhhold==0 & (age>=18 & age<=64));
g standardhhold2=(notstandardhhold==0 & (age>=18 & age<=59));/* Nb: on utilise l'age au moment de l'enquete car d�termine eligibilit� pour la retraite */
replace standardhhold1=0 if occ==21|occ==22|occ==23; /* Les gens qui sont en interruption de travail, quelque soit la raison sont mis en non-standard */
replace standardhhold2=0 if occ==21|occ==22|occ==23;
la var standardhhold1 "Menage standard parent/enfant avec individu entre 18 et 64 ans";
la var standardhhold2 "Menage standard parent/enfant avec individu entre 18 et 59 ans";
local varkeep "`varkeep' propwti propwtm lien standardhhold1 standardhhold2";

/* Sexe */

recode sexe 2=0;
local varkeep "`varkeep' sexe";

/* Education */

replace dipro="" if dipro=="X";
replace dipgen="" if dipgen=="X";
g educa=.;
replace educa=1 if (dipgen=="0"|dipgen=="1") & dipro=="0";
replace educa=2 if (dipro=="1"|dipgen=="2");
replace educa=3 if (dipro=="2"|dipro=="3"|dipro=="4")|(dipgen=="3"|dipgen=="4"|dipgen=="5");
replace educa=4 if (dipro=="6"|dipro=="7"|dipro=="8")|(dipgen=="6"|dipgen=="7"|dipgen=="8");
la var educa "Schooling in 4 levels 1-4 JHS HS A-level Uni";
drop dipgen dipro;
sort hldid persid;
local varkeep "`varkeep' educa";

/* Age, Cohorte */

recode age (0/34=34)(35/59=59)(nonmissing=60), gen(agegr);
gen an_nais= 1900+year-age;
gen cohort= trunc(an_nais/10)*10;
recode cohort ( 1980 1890 1900=1910) (1990=1980);
local varkeep "`varkeep' age an_nais cohort agegr";

/* Enfants: pour les menages non standard => valeurs manquantes */

g nbenfant=nbenf if (lien==0|lien==1) ; //& notstandardhhold==0 ; /* enfants dans le m�nage pour kish=pr ou conjoint */
replace nbenfant=0 if (lien==2) ;//& notstandardhhold==0; /* nombre enfants mis � 0 pour kish enfants de PR ds foyer standard */
gen nbadulte=nb03-nbenf;
g enf4ans=nbe1a+nbe2a+nbe3a+nbe4a if (lien==0|lien==1) ; //& notstandardhhold==0; /* enfants de moins de 4 ans */
replace enf4ans=0 if (lien==2) ;//& notstandardhhold==0;
g enf3ans=(nbe1a+nbe2a+nbe3a>0);
local varkeep "`varkeep' nbenfant nbadulte enf4ans enf3ans";

/*  Situation conjugale */

gen sitconj=1 if cohab==1;
replace sitconj=2 if nbenfant==0 & cohab!=1;
replace sitconj=3 if nbenfant>0 & nbenfant!=. & cohab==2;
label def sitconj 1"Couple" 2 "Seul" 3 "Monoparental", modify;
label val sitconj sitconj;
local varkeep "`varkeep' sitconj";

/* statut d'emploi : Plein, partiel, �tudiant, chomeur, retrait�, autre sans emploi */

g salarie=(statut>=1 & statut<=4);
g independant=(statut==5) ;

recode heurdom 98=. 99=.;
recode heurex 98=. 99=.;
recode mindom 98=. 99=.;
recode minex 98=. 99=.;

g workhours=heurdom+(mindom/60)+heurex+(minex/60);

gen statemploi=6;
replace statemploi=1 if occ==10 & workhours>29; /* Definition OCDE: van Bastelaer, A., G. Lema�tre et P. Marianna (1997),� La d�finition du travail � temps partiel � des fins de
comparaison internationale �, �ditions OCDE. http://dx.doi.org/10.1787/585825266248 - Definition fran�aise: travaille moins que la duree legale du temps de travail */
replace statemploi=2 if occ==10 & workhours<30; //les horaires inconnus ac les temps partiels (28 cas) Il existe une variable modtrav ds la base kish mais du coup on aurait rien sur le conjoint + pb comparabilite avec 2010
replace statemploi=3 if occ==46;
replace statemploi=4 if occ==32;
replace statemploi=5 if occ==31;

label def statemploi 1 "Temps plein" 2 "Temps partiel" 3 "�tudiant" 4 "Ch�meur" 5 "Retrait�" 6 "Autre inactif";
label var statemploi statemploi;
local varkeep "`varkeep' workhours statemploi salarie independant";
compress;


sort hldid persid;
sa "${path}temp", replace;

/* NB : info sur les contraintes pesant sur amenagement du tps de travail peuvent etre obtenues a partir de la base kish */
/* calcul des revenus individuels a partir de la table revenu
attention 9000 valeurs manquantes soit 38% des individus
*/

use "${path}revenu.dta", clear;
destring _all, replace;
rename nquest hldid;
gen double persid=hldid*100+indi; /* indi = nio3 de la base indivi */

merge m:1 persid using "${path}temp";

*revenus individuels annuels;
recode freq191 freq192 freq193 (3=12) (4=1) (8 =0 ) (9 =.), gen(freqr1 freqr2 freqr3);
gen revind=m191*freqr1 + m192*freqr2 + m193*freqr3;
label var revind "revenu individuel annuel";
gen revind_mis=(missing(revind)>0);

*revenus individuels du travail;
	gen revindtrav=m191*freqr1 + m192*freqr2;
	la var revindtrav "revenu individuel annuel du travail";
	gen revindtrav_mis=(missing(revindtrav)>0); /* attention: on utilise le meme nom de variable qu'en 2010 mais definition differente */

*revenu du travail des individus du m�nage, total annuel;
	bys hldid: egen sumrevind=sum(revind);
	bys hldid: egen sumrevind_mis=sum(revind_mis);
	bys hldid: egen sumrevindtrav=sum(revindtrav);
	bys hldid: egen sumrevindtrav_mis=sum(revindtrav_mis);
	bys hldid: egen sumrevtrav=sum(revindtrav!=0 & revindtrav!=.);

	replace sumrevind_mis=(sumrevind_mis>0);
	replace sumrevind=. if sumrevind_mis==1;
	replace sumrevindtrav_mis=(sumrevindtrav_mis>0);
	replace sumrevindtrav=. if sumrevindtrav_mis==1;

	la var sumrevindtrav "Somme des revenus individuels du travail declares";
	la var sumrevindtrav_mis "Menage avec revenu ind travail manquant ds menages";
	la var sumrevind "Somme des revenus individuels declares";
	la var sumrevind_mis "Menage avec revenu ind manquant";

	recode m201 999999=.;
	recode m202 999999=.;
	gen revmen=sumrevind+m201+m202; /* NB: ds la base, sur chaque ligne individuelle figurent les revenus d'allocation du menage... */
	gen revmen_mis=(revmen==.);
	
	la var revmen "Revenus annuels du m�nage";
	la var revmen_mis "Revenu annuels du m�nage manquant";

	recode income 0=. 99=.;
	replace income=1 if revmen<800*12;
	replace income=2 if revmen>=800*12 & revmen<1800*12;
	replace income=3 if revmen>=1800*12 & revmen<3100*12;
	replace income=4 if revmen>=3100*12 & revmen<4500*12;
	replace income=5 if revmen>=4500*12 & revmen<5800*12;
	replace income=6 if revmen>=5800*12 & revmen<7800*12;
	replace income=7 if revmen>=7800*12 & revmen<11000*12;
	replace income=8 if revmen>=11000*12 & revmen<14000*12;
	replace income=9 if revmen>=14000*12 & revmen<18000*12;
	replace income=10 if revmen>=18000*12 & revmen<24000*12;
	replace income=11 if revmen>=24000*12 & revmen<37000*12;
	replace income=12 if revmen>=37000*12 & revmen!=.;
	
	
	/* preparation variables de matching : on matche pour l'essentiel sur le revenu en tranche d�clar� et les caracteristiques de la maison */
	
	recode nation 3=0 9=0;
	recode prepro 23=31 99=0;
	recode preproc 23=31 99=0;
	g csp=int(prepro/10);
	g cspc=int(preproc/10);
	recode surfhab 999=0 .=0;
	g surfhab_mis=(surfhab==0);
	recode nbphab 99=0;
	replace nbphab=6 if nbphab>6 & nbphab!=.;
	recode typjard 9=0;
	recode jardin 2=0 9=0 .=0;
	g ressec=(rs1==1);
	g domestique=(catmen2==1);
	recode stalog 5=3 4=3 6=0 7=0 8=0 9=0;
	foreach suff in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15	{;
		g app`suff'=(nbap`suff'>=1 & nbap`suff'<9);
		g app`suff'2=(nbap`suff'>1 & nbap`suff'<9);
		
																	};
	
	foreach suff in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20	{;
		replace equ`suff'="0" if equ`suff'=="9";
		replace equ`suff'="0" if equ`suff'=="X";
		destring equ`suff', replace;
		g equip`suff'=(equ`suff'>=1 & equ`suff'<9);
		g equip`suff'2=(equ`suff'>1 & equ`suff'<9);
		
																	};
	g telec=(equ01>1 & equ01<9);
	g radio=(equ03>1 & equ03<9);
	
	gen marksample=(income!=. & lien==0);
	ta income, g(inc);
	g nbinf=nbenfant;
	replace nbinf=5 if nbenfant>5 & nbenfant!=.;
	g age10=age/10;
	g age102=(age/10)^2;
	#delimit;
	local varprop "surfhab surfhab_mis app01 app02 app032 app04 app042 app052 app06 app072 app08 app092 app10 app11 app122 app132 app142 app15
					equip01 equip012 equip02 equip022 equip03 equip042 equip05 equip052 equip07 equip08 equip09 equip092
					equip10 equip102 equip12 equip122 equip14 equip15
					i.csp i.cspc i.nation i.nbphab i.typjard ressec domestique i.stalog
					 age age102 i.sitconj i.educa i.nbinf i.sexe i.class i.statemploi i.zeat rural urbain1 urbain2 urbain3 "; /* telec radio congel lavevaisselle */
	xi: logit revmen_mis inc2-inc12 `varprop' if marksample==1;
	cap drop pscore;
	predict pscore if marksample==1;
	set seed 123456789;
	replace revmen=0 if revmen_mis==1 & marksample==1;
	psmatch2 revmen_mis if marksample==1, outcome(revmen) pscore(pscore) ties kernel kerneltype(epan) bwidth(0.01);/* Meilleurs que caliper(0.01) nn(5) et gaussian(0.01) en terme de balancing covariates */
	xi: pstest inc2-inc12 `varprop', both;
	
	su revmen if revmen_mis==0, detail;
	replace revmen=_revmen if marksample==1 & revmen_mis==1 & _support==1;
	g revmen_imp=(marksample==1 & revmen_mis==1 & _support==1 & _revmen!=.);
	su revmen if revmen_imp==1, detail;
	sort hldid persid;
	replace revmen=revmen[_n-1] if hldid==hldid[_n-1] & revmen==.;
	replace revmen_mis=0 if revmen_imp==1;
	
	
/*
	gen logrevmen=ln(revmen/12);
	bys hldid: g a=(_n==1);
	reg logrevmen if a==1, cluster(hldid);
	local sigma=e(rmse);
	predict m, xb;
	qui su m;
	local moyenne=r(mean);
	qui drop m a;
	local m=exp(`moyenne'+`sigma'^2/2);

	recode income 0=. 99=.;
	g revmen_imp=(revmen==. & income!=.);
	replace revmen_mis=0 if revmen_imp==1;
	
	g b0=(ln(800)-`moyenne')/`sigma' if income==1;
	replace b0=(ln(1800)-`moyenne')/`sigma' if income==2;
	replace b0=(ln(3100)-`moyenne')/`sigma' if income==3;
	replace b0=(ln(4500)-`moyenne')/`sigma' if income==4;
	replace b0=(ln(5800)-`moyenne')/`sigma' if income==5;
	replace b0=(ln(7800)-`moyenne')/`sigma' if income==6;
	replace b0=(ln(11000)-`moyenne')/`sigma' if income==7;
	replace b0=(ln(14000)-`moyenne')/`sigma' if income==8;
	replace b0=(ln(18000)-`moyenne')/`sigma' if income==9;
	replace b0=(ln(24000)-`moyenne')/`sigma' if income==10;
	replace b0=(ln(37000)-`moyenne')/`sigma' if income==11;

	g a0=(ln(800)-`moyenne')/`sigma' if income==2;
	replace a0=(ln(1800)-`moyenne')/`sigma' if income==3;
	replace a0=(ln(3100)-`moyenne')/`sigma' if income==4;
	replace a0=(ln(4500)-`moyenne')/`sigma' if income==5;
	replace a0=(ln(5800)-`moyenne')/`sigma' if income==6;
	replace a0=(ln(7800)-`moyenne')/`sigma' if income==7;
	replace a0=(ln(11000)-`moyenne')/`sigma' if income==8;
	replace a0=(ln(14000)-`moyenne')/`sigma' if income==9;
	replace a0=(ln(18000)-`moyenne')/`sigma' if income==10;
	replace a0=(ln(24000)-`moyenne')/`sigma' if income==11;
	replace a0=(ln(37000)-`moyenne')/`sigma' if income==12;

	replace revmen=12*`m'*normal(b0-`sigma')/normal(b0) if income==1 & revmen_imp==1;
	replace revmen=12*`m'*(1-normal(a0-`sigma'))/(1-normal(a0)) if income==12 & revmen_imp==1;
	replace revmen=12*`m'*(normal(b0-`sigma')-normal(a0-`sigma'))/(normal(b0)-normal(a0)) if income>1 & income<12 & revmen_imp==1;
*/
	g oktrav=(sumrevtrav>=nbtravailleur & sumrevtrav!=.); /* variable indiquant si le nbr de revenus du travail collect� est �gal au nombre de travailleurs d�clar�s) */
	g revmenhorstrav_mis=(revmen<sumrevindtrav|revmen==.|sumrevindtrav==.|sumrevindtrav_mis>0|oktrav!=1);
	g double revmenhorstrav=revmen-sumrevindtrav;
	replace revmenhorstrav=0 if revmenhorstrav_mis==1;
	la var revmenhorstrav "Unearned yearly household income; 0 if missing";
	la var revmenhorstrav_mis "Unearned yearly household income missing";


g wagerate=((revindtrav/52)/workhours);
replace wagerate=0 if statemploi==4|statemploi==5|statemploi==6; 
/* Va falloir aussi virer les etudiants et les independants */
replace wagerate=(wagerate/6.55957)*(107.8186/66.03263) if year==85;
replace wagerate=(wagerate/6.55957)*(107.8186/67.70889) if year==86;
replace revmenhorstrav=(revmenhorstrav/6.55957)*(107.8186/66.03263) if year==85;
replace revmenhorstrav=(revmenhorstrav/6.55957)*(107.8186/67.70889) if year==86;
replace revmen=(revmen/6.55957)*(107.8186/66.03263) if year==85;
replace revmen=(revmen/6.55957)*(107.8186/67.70889) if year==86;
label var revmen "Revenu annuel m�nage euro2010" ;
local varkeep "`varkeep' revindtrav revindtrav_mis sumrevindtrav sumrevindtrav_mis 
sumrevtrav oktrav revmenhorstrav revmenhorstrav_mis wagerate revmen revmen_mis revmen_imp ";

duplicates tag hldid persid, gen(i);
*replace  /*somme_revind */ sumrevind=. if i>0; //je suppose que c'est sumrevind qu'il faut lire;
replace  revindtrav=. if i>0;

bys hldid persid: keep if (_n==1);
drop i;

keep hldid persid revindtrav revindtrav_mis sumrevindtrav sumrevindtrav_mis 
					sumrevtrav oktrav revmenhorstrav revmenhorstrav_mis wagerate revmen revmen_mis revmen_imp;
sort hldid persid;
save "${path}revind.dta", replace;

u "${path}temp", clear;
sort hldid persid;
merge 1:1  persid using "${path}revind.dta";
keep if _merge==1|_merge==3;
cap drop _merge;
g wage1=wagerate if statemploi==1|statemploi==2;
replace wage1=. if statemploi>2;
local varkeep "`varkeep' wage1";
compress;
sort hldid persid;
sa "${path}temp", replace;


/* Instruments */

	/* garde enfants */
	
	g aideenf=0;
	g aide3=0;
	replace aideenf=1 if mena1==1|mena3==1|mena4==1;
	replace aide3=1 if aida03==1|aidb03==1|aidc03==1;
	
	la var aideenf "aide non payante pour garder les enfants au domicile";
	la var aide3 "a ete aide occasionnellement par voisins, amis ou parents pour garde enfant"; /* definition un peu differente que ds 2009 */
	
	g nochildcareexp=(aidd01!=1 & aidd02!=1 & aidd03!=1 & aidd04!=1 & mena2!=1);
	g childcareexpgeo1=(1-nochildcareexp)*rural;
	g childcareexpgeo2=(1-nochildcareexp)*(paris+urbain3);
	
	local varkeep "`varkeep' aide3 aideenf nochildcareexp childcareexpgeo1 childcareexpgeo2";
	
	/* Caracteristiques emploi actuel: pour equation de salaire - peu de variables renseignees malheureusement */
	
	recode class 9=0 7=1;
	ta class, g(class);
	
	local varkeep "`varkeep' class2-class7";
	
	/* Eloignement contraint de l'emploi: chomeurs ou ancien actif contraint a etre eloigne*/
	
	recode ancess 99=. 98=.;
	g ancienact=(occ==21|occ==22|occ==23|occ==33|occ==34|occ==35);
	for num 1/3: g eloignX=0;
	replace eloign1=(year-ancess<1 & year-ancess>=0) if ancienact==1|statemploi==4;
	replace eloign2=(year-ancess<2 & year-ancess>=1) if ancienact==1|statemploi==4;
	replace eloign3=(year-ancess<3 & year-ancess>=2) if ancienact==1|statemploi==4;
	
	local varkeep "`varkeep' ancienact eloign1 eloign2 eloign3";
	
	/* CSP des parents - notamment m�ere au foyer */
	
	g mereinactive=(promer==85) if lien==0;
	replace mereinactive=(promerc==85) if lien==1;
	g mereinactivemissing=(mereinactive==.);
	replace mereinactive=0 if mereinactivemissing==1;
	
	
	local varkeep "`varkeep' mereinactive";
	
	/* Variables croisees: cohortes*csprisk; revenu*structfam */
	 g cohort1=(an_nais<1945);
	 g cohort2=(an_nais>=1945 & an_nais<1960);
	 g cohort3=(an_nais>1960 & an_nais<1975);
	 g cohort4=(an_nais>1975);
	 
	 g csprisk1=inlist(pcs, 10, 11, 12, 13, 21, 22);
	 g csprisk2=inlist(pcs, 23, 31, 35, 37, 38);
	 g csprisk3=inlist(pcs, 46, 47, 48); 
	 g csprisk4=(pcs>53 & pcs<70); /* NB: categorie de reference implicite = metiers proteges des fluctuations economiques */
 
	 local i=1;
	 forv j=1/4	{;
	  	forv k=1/4	{;
	 	 	g interacteco`i'=csprisk`k'*cohort`j';
			local varkeep "`varkeep' interacteco`i'";
	 	 	local i=`i'+1;
	 	 			};
	 	 		};
	 local varkeep "`varkeep' csprisk1 csprisk2 csprisk3 csprisk4";
	 		
	 g struct1=(enf3ans>0 & nbe6am>0);
	 g struct2=(enf3ans==0 & nbe6am>0);
	 g struct3=(enf3ans==0 & nbe6am==0 & nbenfant>0);
	 g struct4=(enf3ans==0 & nbe6am==0 & nbenfant==0);	
	 qui su revmen, detail;
	 local i=1;
	 g seuil=0;
	 forv j=1/4		{;
	 	foreach v in 25 50 75 {;
	 		replace seuil=(revmen>r(p`v'));
	 		g interacttax`i'=struct`j'* seuil;
			local varkeep "`varkeep' interacttax`i'";
	 		local i=`i'+1;
	 	 						};
	 	 			};
	 local varkeep "`varkeep' struct1 struct2 struct3 struct4";
	 
di "`varkeep'";
rename lien lienpref;
global varkeep "`varkeep'";
keep  `varkeep' lienpref;
sort hldid persid;

sa "${path}temp", replace;
					
					
# delimit ;
/* emploi du temps */
use "${path}carnetdestr.dta", clear;
*destring _all, replace;

rename nquest hldid;
gen double persid=hldid*100+nib2;
replace oub2="." if oub2=="X";
destring oub2, replace;


	local varlist repas repastra repasdom repasres repasaut repasprinc repasprincsssec repasprincsec repasprincvid 
		repasseulprinc repassecseul repassec repassecvid cuisine cuisineprinc cuisineprincsec cuisineprincsssec  
		cuisineprincvid cuisineprincseul cuisinesec cuisinesecvid sport etude repastraprinc repasdomprinc 
		repasresprinc repasautprinc repaspartprinc repasdompartprinc repastrapartprinc repasextpartprinc 
		repasdomseulprinc repastraseulprinc repasextseulprinc repasdomfamprinc repastrafamprinc repasextfamprinc 
		repasfamprinc repasextprinc mealchores travailsal;
	
	local Lrepas "(primb2>140 & primb2<170)|(secb2>140 & secb2<170)";
	local Lrepastra " (primb2==151 | secb2==151) |(((primb2>=152 & primb2<=154)|(secb2>=152 & secb2<=154)) & (oub2==1|quib2d==5)) ";
	local Lrepasdom "(primb2>140 & primb2<150)|(secb2>140 & secb2<150)";
	local Lrepasres " (((primb2>=152 & primb2<=154)|(secb2>=152 & secb2<=154)) & (oub2!=1 & quib2d!=5))
					|(primb2>155|primb2==156|secb2==155|secb2==156)";
	local Lrepasaut " ((primb2>160 & primb2<170)|(secb2>160 & secb2<170))";
	local Lrepasprinc "(primb2>140 & primb2<170)";
	local Lrepasprincsssec " (primb2>140 & primb2<170) & secb2==0 ";
	local Lrepasprincsec " (primb2>140 & primb2<170) & secb2!=0 ";
	local Lrepasprincvid " (primb2>140 & primb2<170) & ((secb2>710 & secb2<720)|secb2==748) ";
	local Lrepasseulprinc " (primb2>140 & primb2<170) &  quib2d==0 ";
	local Lrepassecseul " (secb2>140 & secb2<170) & quib2d==0 ";
	local Lrepassec " (secb2>140 & secb2<170)  ";
	local Lrepassecvid "(secb2>140 & secb2<170) & ((primb2>710 & primb2<720)|primb2==748)  ";
	local Lcuisine "(primb2==311|primb2==312)|(secb2==311|secb2==312)";
	local Lcuisineprinc "(primb2==311|primb2==312)";
	local Lcuisineprincsec " (primb2==311|primb2==312) & secb2!=0";
	local Lcuisineprincsssec " (primb2==311|primb2==312) & secb2==0 ";
	local Lcuisineprincvid " (primb2==311|primb2==312) & ((secb2>710 & secb2<720)|secb2==748) ";
	local Lcuisineprincseul " (primb2==311|primb2==312) & quib2d==0 ";
	local Lcuisinesec " (secb2==311|secb2==312) ";
	local Lcuisinesecvid " (secb2==311|secb2==312) &  ((primb2>710 & primb2<720)|primb2==748) ";
	local Lsport "(primb2>=611 & primb2<=614)|(secb2>=611 & secb2<=614)";
	local Ltravailsal "(primb2>=211 & primb2<246)| (primb2>=811 & primb2<=815)|(primb2==841) | primb2==151 | primb2==153";
	local Letude " (primb2>251 & primb2<254)  ";
	local Lrepastraprinc  " primb2==151 | primb2==153 | ((primb2==152 | (primb2>=154 & primb2<160)) & (oub2==1|quib2d==5)) ";
	
	local Lrepasdomprinc "(primb2>140 & primb2<150) | inrange(primb2, 164, 166)";
	local Lrepasresprinc " (primb2==152|(primb2>=154 & primb2<164)) ";
	local Lrepasautprinc " ((primb2>140 & primb2<150)|(primb2>163 & primb2<170)) & (quib2d>3 & quib2d<10) ";
								
	local Lrepaspartprinc  " (primb2>140 & primb2<170) & quib2d!=0 ";
	local Lrepasdompartprinc " ((primb2>140 & primb2<150) | inrange(primb2, 164, 166)) & quib2d!=0 & oub2==0 ";
	local Lrepastrapartprinc " (primb2==151 |((primb2>=152 & primb2<=154) & (oub2==1|quib2d==5))) & quib2d!=0" ;
	local Lrepasextpartprinc "  ((primb2>160 & primb2<164)| ( (primb2==152 | (primb2>=154 & primb2<160)) & quib2d!=5 & oub2!=1)) & quib2d!=0 & quib2d!=5  " ;
	local Lrepasdomseulprinc "  (primb2>140 & primb2<170) & quib2d==0 & oub2==0  " ;
	local Lrepastraseulprinc " (primb2==151 |((primb2>=152 & primb2<=154) & oub2==1)) & quib2d==0 " ;
	local Lrepasextseulprinc " ((primb2>160 & primb2<164)| ( (primb2==152 | (primb2>=154 & primb2<160)) & quib2d!=5 & oub2!=1)) & quib2d==0 " ;
	local Lrepasdomfamprinc " ((primb2>140 & primb2<150) | inrange(primb2, 164, 166)) & quib2d>0 & quib2d<4 & oub2==0 ";
	local Lrepastrafamprinc "  (primb2==151 |((primb2>=152 & primb2<=154) & oub2==1)) & quib2d>0 & quib2d<4 " ;
	local Lrepasextfamprinc " ((primb2>160 & primb2<164)| ( (primb2==152 | (primb2>=154 & primb2<160)) & quib2d!=5 & oub2!=1)) &  quib2d>0 & quib2d<4 " ;
	local Lrepasfamprinc " (primb2>140 & primb2<170) & quib2d>0 & quib2d<4 ";
	local Lrepasextprinc "  (primb2>160 & primb2<164)| ( (primb2==152 | (primb2>=154 & primb2<160)) & quib2d!=5 & oub2!=1) " ;
	local Lmealchores "primb2>312 & primb2<320 " ;


	local varcoll " ";
	foreach X in `varlist' {;
		g `X'=0;
		replace  `X'=duree if `L`X'' ;
		g n`X'=0;
		sort hldid persid;
		replace n`X'=1 if `X'>0 & `X'[_n-1]==0 & hldid==hldid[_n-1] & persid==persid[_n-1];
		local varcoll "`varcoll' `X' n`X'";
			};


sort hldid persid;

collapse (sum) `varcoll', by(hldid persid);

sort hldid persid;
save  "${path}EDT85_France_carnet.DTA", replace;

*merge les fichiers individu et carnet;
merge 1:1 hldid persid using "${path}temp.dta";
keep if _merge==3;
compress;
cap drop _merge;
save "${path}EDT85_France_janvier2013.dta", replace;
exit, clear;


