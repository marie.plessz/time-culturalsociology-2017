/**********************
Projet	TIME
Auteur	MP
date	06/04/2016
Tache
	master do file
	lance tous les dofiles utiles.
************************/
set more off

*pr�parer les donn�es de chaque pays
do "$do/TMP_US_0_master"		// pr�parer data US
do "$do/TMP_Fce_0_master"		// pr�parer data Fce

* append et recodages sur data fusionn�es
do "$do/TMP_1_append"			// s�lectionner vars et append les 2 fichiers
do "$do/TMP_2_recode"			// recoder le fichier append
