/********************
Projet : 	Time
auteur : 	Marie
date : 		06/04/2016

t�che: 
. mettre bout � bout les bases fran�aises et US
*********************/

* pr�parer le fichier USA all�g�
use "$data/USAestsample_hld.dta", clear 


keep hldid persid sexe sumcuisineprinc  cuisineprinc aged* college worker pstatus ///
	single kids  we nhomemeals homem* nrepasdomprinc poidsredressement2 ///
	pworker enquete old1985	nrepasprinc nrepasextprinc somecooking
save "$data/temp_append_US", replace

* pr�parer le fichier Fce all�g�
use "$data/mp_Fce_4_Estsample.dta", clear
 
 
keep hldid persid sexe sumcuisineprinc  cuisineprinc aged* college worker pstatus ///
	kids single pworker we nhomemeals homem* nrepasdomprinc 	///
	poidsredressement2 	enquete old1985	nrepasprinc nrepasextprinc	///
	somecooking microond lavevaisselle congel revclass
save "$data/temp_append_Fce", replace



* append et cr�e var country

use  "$data/temp_append_Fce", clear
gen country=0
append using "$data/temp_append_US"
tab country, mis
replace country=1 if country==.
label var country "Country"
label def country 0 "0_France" 1 "1_USA", modify
label val country country

save "$data/TMP_1_append", replace
