*EDT USA 2010
*r�vis� le 18 juillet 2014
/*	r�alise : 
	-simulation temps cuisine conjoint
	-fusion avec donn�es enqu�t�s
*/


set more off

use  "$data/MTUS_USA_janv2013.DTA", clear
	
	
	*****************************************************
	*	simulation du temps de cuisine des conjoints	*
	*****************************************************

	/* selection des couples */
	save "$data/temp.DTA", replace
	keep if survey==2003
	keep if sitconj==1



	/*creation du fichier avec une ligne par enqu�t� atus et une ligne pour le conjoint*/
	save "$data/temp_cj.DTA", replace

	expand 2 , gen(sim) /* sim vaut 1 si l'observation a �t� cr��e */
	label var sim "Simulated time values"

	*quietly : recode  nrepas-repasextprinc (nonmissing=.) if sim==1

	*rename revindsp revindtravsp
	*rename revindsp_mis revindtravsp_mis

	replace sexesp=1 - sexe 

	local var "sexe statemploir  revindtrav      heursem     age         wagerate      white       hispanic    educ educa"

	foreach x of local var {
		gen `x'1= `x' *(1-sim) + `x'sp * (sim)
		gen `x'2= `x' *(sim) + `x'sp * (1-sim)
	}	

	foreach x of local var {
		replace `x'= `x'1 
		gen p`x'= `x'2
		drop `x'1 `x'2
	}

	local mvar "revindtrav salhor wagerate heursem" 
	foreach x of local mvar {
		gen `x'_mis1= `x'_mis *(1-sim) + `x'sp_mis * (sim)
		gen `x'_mis2= `x'_mis *(sim) + `x'sp_mis * (1-sim) 
	}
	local mvar "revindtrav salhor wagerate heursem" 
	foreach x of local mvar {
		replace `x'_mis= `x'_mis1 
		gen p`x'_mis= `x'_mis2
		drop `x'_mis1 `x'_mis2
	}

	/* quelques recode */


	g age10=10*int(age/10)
	recode age10 (10=20) (80=70)

	recode nbenfant (4/max=4), gen(nbenfr)
	recode agekid2 (25/max=25), gen(agekid3)
	recode day (2/6=0) (1/7=1) , gen(we)
	recode agekidx (-7=0), gen(agekidx2)
local vardrop " nbenfr agekid3 we agekidx2"

		#delimit ;
	foreach var in cuisineprinc repasprinc repasdomprinc repasextprinc repastraprinc repasdomfamprinc  { ;
		_pctile `var' , p(98) ;
		gen `var'o2=`var' ;
		replace `var'o2=. if `var'>r(r1) & `var'!=. ;
		local vardrop " `vardrop'  `var'o2" ;
	} ;
	# delimit cr
	

	/*	variables	*/
	recode educa (0=.) (1=2) (7=6), gen(educar)
	recode peduca (0=.) (1=2) (7=6), gen(peducar)
	local vargen "educar peducar age10 gereg agekidx2"
	foreach var of local vargen {
		quietly: tab `var', gen(`var') 
	}
local vardrop " `vardrop' educar* peducar* age10*	gereg1-gereg4 agekidx2* "

	/*	propensity score matching */
	/* on s'en fiche que la s�lection soit al�atoire, le mod�le va rapprocher des individus qui se ressemblent par leur vecteur X
	Le PSM a l'avantage qu'il ne suppose aucun "mod�le" on n'a pas � se demander si les choses sont endog�nes ou quelle est la forme
	de la courbe
	*/
	local varbase " educar2-educar5  age102- age106  gereg2- gereg4 white hispanic urban2 we "
	local var2 "peducar2- peducar5 heursem heursem_mis pheursem pheursem_mis revindtrav revindtrav_mis previndtrav previndtrav_mis  "
	/*
	forvalues s=0/1 {
		di in  "SEXE = `s'"
		psmatch2 sim `varbase' `var2' if sexe==`s' , outcome(cuisineprinco2)  logit common llr trim(0.03) kerneltype(biweight) bwidth(0.06)
		pstest `varbase' `var2', both
		replace simcuisine=_cuisineprinco2 if sexe==`s'
		psgraph
	}
	*/
	*essai avec des "strates"
	 egen g = group(statemploir pstatemploir sexe we )

	 
	 cap drop simcuisine c scuisine
	gen simcuisine=.
	levels g, local(gr)
	tab g
	set seed 123456789
	local varbase " educar2-educar5  age102- age106  gereg2- gereg4  hispanic urban2 white "
	local var2 " peducar2- peducar5 heursem heursem_mis pheursem pheursem_mis revindtrav revindtrav_mis previndtrav previndtrav_mis  "
	qui foreach j of local gr {
					psmatch2 sim `varbase' `var2' if g==`j', out(cuisineprinco2)  logit common  
					replace simcuisine = _cuisineprinco2 if  g==`j'
			}

	/*	quelques v�rifications 	*/
	/*
	sum simcuisine, det
	gen c=cuisineprinc if sim==0  
	replace c=simcuisine if sim==1
	replace c=0 if c<0
	 /*c=temps de cuisine pour la ligne, qu'elle soit simul�e ou non	*/
	*attention c vaut cuisineprinc (donc prend des valeurs extr�mes pour les individus observ�s

	gen cuisine2010=c	/*pour les v�rif, utiliser q qui est manquant pour ego et son conjoint simul� si cuisineprinc est dans les 2 derniers pctiles) */
	replace cuisine2010=. if cuisineprinco2==.
	histogram cuisine2010, by(sim sexe ) scheme(s2mono) percent xlabel(0(60)240) ytitle("%")

	local varbase " educar2-educar5  age102- age106  gereg2- gereg4 white hispanic urban2 we "
	local var2 "statemploir pstatemploir  peducar2- peducar5 heursem heursem_mis pheursem pheursem_mis revindtrav revindtrav_mis previndtrav previndtrav_mis  "
	regress cuisine2010 `varbase' `var2' if sim==0  
	eststo A
	regress cuisine2010 `varbase' `var2' if sim==1 
	eststo B
	esttab A B, wide

	bys persid : egen scuisine=sum(c)
	*replace scuisine==. if cuisineprinco2==.

	sum scuisine, det 
	replace scuisine=. if scuisine>r(p99) /* je code manquant les temps de cuisine de couples sup�rieur au 99� pctile	*/
	sum scuisine, det 
	hist scuisine if sexe==0, xlabel(0(30)240) xtitle("Cooking time (min)") scheme(s2mono) percent	ytitle("%")

	gen d=scuisine==0
	tab d if sexe==0
	drop d
	*/
	display "`vardrop'"
	drop `vardrop'
	drop   _pscore - _pdif  g
			
	save "$data/temp_cj.DTA", replace

	*********************************************************************
	/*	cr�ation du fichier de data avec les temps de cuisine m�nage	*/
	*********************************************************************

	use "$data/temp.DTA", clear
	drop if survey==2003 & sitconj==1
	append using "$data/temp_cj.DTA"
	save  "$data/MTUS_USA_impute_juin2013.DTA", replace

	gen double i=hldid
	replace i=persid if survey==2003
	duplicates tag i, gen(e)

	gen k=relrefp>2
	sort survey i k sexe
	bys survey i k : gen j=_n
	tab j sitconj
	gen q=cuisineprinc
	replace q=simcuisine if sim==1
	replace q=. if atypique==1
	replace q=. if sitconj==1 & e==0
	replace q=. if sitconj==1 & j==1 & sexe==1
	replace q=. if sitconj==1 & j==2 & sexe==0

	bys survey i : egen sumcuisineprinc=sum(q)
	replace sumcuisineprinc=. if q==.

	/*	garder 1 ligne par m�nage	*/
	keep if j==1
	save  "$data/MTUS_USA_impute_juin2013.DTA", replace

	sum sumcuisine, det 
	replace sumcuisine=. if sumcuisine>r(p99) & sumcuisine!=. /* je code manquant les temps de cuisine de couples sup�rieur au 99� pctile	*/
	sum sumcuisine, det 

recode day (2/6=0) (1/7=1) , gen(we)

	*hist sumcuisine , xlabel(0(30)240) xtitle("Cooking time (min)") scheme(s2mono) percent	ytitle("%") by(survey sitconj )

*egen z=cut(sumcuisineprinc), at(0,1,15,30,60,90,120,180,240)
*z sert � faire des graphiques sur excel
cap drop i e k j q 


/*	remise en place des var pour les conjoints	*/
rename sex sexold

foreach var in sexe statemploir  revindtrav      heursem     age  wagerate   educ educa	{
	replace p`var' = `var'sp if survey==1985
	replace p`var'=0 if p`var'==. & sitconj!=1
}

foreach var in revindtrav salhor wagerate heursem  {
	replace p`var'_mis = `var'sp_mis	
}	

compress
save  "$data/MTUS_USA_impute_juin2013.DTA", replace
