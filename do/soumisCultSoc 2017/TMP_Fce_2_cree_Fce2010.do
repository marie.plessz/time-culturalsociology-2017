*	pr�paration des donn�es de 2010 Emploi du temps france
*projet TIME, Fabrice Etil�
*revu par Marie le 18juillet 2014

version 11.0
set more off
# delimit;
*global path "C:\DATA\TIME\EDT2009\";
global path "$data\EDT2009\";
*global path "/Users/fabriceetile/Documents/Fabrice - Work/DATA/TIME/EDT2009/";
cd "$path";


cap log cl;
set logtype t;
log using makedataEDT2009, replace;
di in b "This program was run at $S_TIME on $S_DATE";

clear;
set more 1;
set matsize 800;
set mem 500m;

/* creation d'une variable indiquant si l'individu est dans un menage non standard, 
 cad peut e�tre un adulte autre que celui s'occupant des enfants (pere, mere ou tuteur principal) etc + nombre d'enfants */
/* comptatge des enfants de 1 an, 2 ans, 3 ans, 4 ans et 6 ans et moins*/

use "${path}hab_menage.dta", clear;
keep idmen idind lienpref age actoccup; 
destring _all, replace;
rename idmen hldid;
rename idind persid;
sort hldid;
recode actoccup 2=0 .=0;
preserve;
keep hldid persid actoccup;
sort hldid persid;
sa "${path}actoccup.dta", replace;
restore;
sort hldid;
egen nbtravailleur=sum(actoccup), by(hldid);
ta nbtravailleur;
gen a= (lienpref>2) ;
bys hldid: egen b=sum(a);
gen notstandardhhold=b>0;
drop a b;

gen e1a=age<=1;
gen e2a=age==2;
gen e3a=age==3;
gen e4a=age==4;
gen e6am=age<6;
foreach var of varlist e1a-e6am {;
	bys hldid: egen nb`var'=sum(`var');
}	;

bys hldid: keep if _n==1;
local varkeep "hldid notstandardhhold  nbe1a nbe2a nbe3a nbe4a nbe6am nbtravailleur";
keep `varkeep';
sort hldid ;
sa  "${path}temp.dta", replace;


/* recuperation des variables uniquement presentes dans la base menage */

use "${path}menage.dta", clear;
keep idmen pond_menage nenfants agepr agecj nhab pond_menage mrd2 mrduc revenu_men_cor rev_uc zeat tuu1999 
			gardh scol1-scol9 gardep1-gardep9 mercrem1-mercrem9 soirm1-soirm9 mercreg1-mercreg9 soirg1-soirg9 
			aid06 aid06f serv33 serv33f serv33p serv32 serv32f serv32p log* revenu_men_cor; 
destring _all, replace;

rename idmen hldid;
sort hldid;
merge 1:1 hldid using "${path}temp.dta";
keep if _merge==3;
cap drop _merge;
bys hldid: keep if _n==1;
rename tuu1999 tuu;
g rural=(tuu==0);
g urbain1=(tuu==1|tuu==2|tuu==3);
g urbain2=(tuu==4|tuu==5);
g urbain3=(tuu==6|tuu==7);
g paris=(tuu==8);

ta nbtravailleur;
g microonde=(log110==1);
replace microonde=0 if log110==2;
g lavevaisselle=(log111==1);
replace lavevaisselle=0 if log111==2;
g congel=(log113==1);
replace congel=0 if log113==2;
drop log*;
rename revenu_men_cor revmen; 
replace revmen=revmen*12;
g revmen_mis=(revmen==.);
label var revmen_mis "Revenus du menage manquant";
la var revmen "Revenus annuels du menage";
rename pond_menage propwtm;
local varkeep "`varkeep' tuu rural urbain1 urbain2 urbain3 paris microonde zeat
				lavevaisselle congel revmen revmen_mis propwtm";
sort hldid;
sa  "${path}temp.dta", replace;

/* Caracteristique de la Journee enquetee */
/* recuperation des infos sur le jour echantillone et savoir si exceptionnel ou non */

use "${path}carnetdestr.dta", clear;
keep idind idmen exej typj jour mois_carnet pond_carnet annee_carnet;
*destring _all, replace;

rename idind persid;
rename idmen hldid;
sort hldid persid;
merge m:1 hldid using "${path}temp";
keep if _merge==3;
cap drop _merge;


rename exej abnormal;
g regularworkday=(typj==1) if typj!=7;
g regulardayoff=(typj==3|typj==6) if typj!=7;
la var regularworkday "Regular work day for workers";
la var regulardayoff "Regular day off for workers";
la var abnormal "Abnormal day";

g day=1 if jour=="lundi";
replace day=2 if jour=="mardi";
replace day=3 if jour=="mercredi";
replace day=4 if jour=="jeudi";
replace day=5 if jour=="vendredi";
replace day=6 if jour=="samedi";
replace day=7 if jour=="dimanche";  
rename mois_carnet month;
rename annee_carnet year;

local varkeep "`varkeep' persid abnormal regulardayoff regularworkday day month year";
sort hldid persid;
sa  "${path}temp.dta", replace;


/* recuperation des variables individuelles */

use "${path}individu.dta", clear;
keep idind idmen pond_qi  diplom* sexe age couple tem4 grignot situa actif actoccup act1a act1m typemploi situa statut act21tot act20 act2 tpp lienpref ina1 
		red_indrev indrev red_secrev secrev red_salrev salrev cs24 cs42 typhor typemploi
		txtppb tpp travh cess3 cess2 cho1 vacantrav etetrav prof courh copieh suivih parenth colleh autreh docuh hremh txtppb stplc stmn 
		fim anaip anaim merdipl perdipl stig* lienpref;
destring _all, replace;

rename idmen hldid;
rename idind persid;
sort hldid persid;
merge 1:m hldid persid using "${path}temp";
keep if _merge==3;
cap drop _merge;

/* Echantillon */

rename pond_carnet propwti; /* Comment: faudra voir ce qu'on fait des ponderations */
g standardhhold1=(notstandardhhold==0 & (age>=18 & age<=64));
g standardhhold2=(notstandardhhold==0 & (age>=18 & age<=59));/* Nb: on utilise l'age au moment de l'enquete car d�termine eligibilit� pour la retraite */
replace standardhhold1=0 if ina1!=.; /* Les gens qui sont en interruption de travail, quelque soit la raison sont mis en non-standard */
replace standardhhold2=0 if ina1!=.;
la var standardhhold1 "Menage standard parent/enfant avec individu entre 18 et 64 ans";
la var standardhhold2 "Menage standard parent/enfant avec individu entre 18 et 59 ans";
local varkeep "`varkeep' propwti lien standardhhold1 standardhhold2";

/* Sexe */

recode sexe 2=0;
local varkeep "`varkeep' sexe";
label var sexe "Male?"	;
label def sexe 0 "0_Female" 1 "1_Male", modify	;
label val sexe sexe	;
/* Education */

g educa=.;
replace educa=1 if (diplom11==1|diplom12==1|diplom13==1|diplom14==1|diplom15==1|diplom21==1);
replace educa=2 if (diplom31==1|diplom41==1|diplom42==1|diplom43==1|diplom44==1);
replace educa=3 if (diplom51==1|diplom52==1|diplom53==1|diplom54==1|diplom61==1);
replace educa=4 if (diplom71==1|diplom72==1|diplom73==1|diplom74==1)|(diplom81==1|diplom82==1|diplom83==1|diplom84==1|diplom85==1);
la var educa "Schooling in 4 levels 1-4 JHS HS A-level Uni";
drop diplom*;
sort hldid persid;
local varkeep "`varkeep' educa";

/* Age, Cohorte */

recode age (0/34=34)(35/59=59)(nonmissing=60), gen(agegr);
gen an_nais= year-age;
gen cohort= trunc(an_nais/10)*10;
recode cohort (1890 1900=1910) (1990=1980);
local varkeep "`varkeep' age an_nais cohort agegr";

/* Enfants: capture la contrainte pesant sur la PR ou son conjoint uniquement du menage: pas sur les autres adultes??? */

g nbenfant=nenfants if (lienpref==0|lienpref==1) ; //& notstandardhhold==0 ; /* enfants dans le m�nage pour kish=pr ou conjoint */
replace nbenfant=0 if (lienpref==2) ;//& notstandardhhold==0; /* nombre enfants mis � 0 pour kish enfants de PR ds foyer standard */
gen nbadulte=nhab-nenfants;
g enf4ans=nbe1a+nbe2a+nbe3a+nbe4a if (lien==0|lien==1) ; //& notstandardhhold==0; /* enfants de moins de 4 ans */
replace enf4ans=0 if (lien==2) ;//& notstandardhhold==0;
g enf3ans=(nbe1a+nbe2a+nbe3a>0);


local varkeep "`varkeep' nbenfant nbadulte enf4ans enf3ans";

/*  Situation conjugale */

gen sitconj=1 if couple==1;
replace sitconj=2 if nbenfant==0 & couple!=1;
replace sitconj=3 if nbenfant>0 & nbenfant!=. & (couple==2|couple==3); /* inclus dans monoparental les gens en couple avec qqun hors logement et un enfant */
label def sitconj 1"Couple" 2 "Seul" 3 "Monoparental", modify;
label val sitconj sitconj;
local varkeep "`varkeep' sitconj";

/* statut d'emploi : Plein, partiel, �tudiant, chomeur, retrait�, autre sans emploi */

g salarie=(statut>=1 & statut<=4);
g independant=(statut==6|statut==7); 
g workhours=travh;
/*replace workhours=workhours+(vacantrav*3)*5/52 if prof!=1; a discuter: porte sur peu d'effectifs */
replace workhours=(courh + copieh + suivih + parenth + colleh + autreh + docuh)+hremh if prof==1;
replace workhours=workhours+(etetrav*8)/52+(vacantrav*3)*7/44 if prof==1; /* Hyp: 1/2 j = 3h travaillee, 7 sem de vacances, a rerepartir sur 44 semaines */

gen statemploi=6;
replace statemploi=1 if situa==1 & workhours>29; /* typemploi==6*/
replace statemploi=2 if situa==1 & workhours<30; //Attention: & (typemploi==7|tpp==2) & txtppb<4 probleme dans le data model et la variable TPP: variable peu fiable en 2009-2010
replace statemploi=3 if situa==2|situa==3; // inclus les apprentis
replace statemploi=4 if situa==4;
replace statemploi=5 if situa==5;

label def statemploi 1 "Temps plein" 2 "Temps partiel" 3 "�tudiant" 4 "Ch�meur" 5 "Retrait�" 6 "Autre inactif";
label var statemploi statemploi;

local varkeep "`varkeep' workhours statemploi typhor typemploi salarie independant";


/* Choix horaire: contrainte subjective */

g WTconstrpos=(stplc==1);
g WTconstrneg=(stplc==2 & stmn==1);
la var WTconstrpos "Working Time cannot be increased";
la var WTconstrneg "Working Time cannot be decreased";
local varkeep "`varkeep' WTconstrpos WTconstrneg";

/* Revenus */

g redindrev0=red_indrev;
replace redindrev0=0 if red_indrev==.;
g redsecrev0=red_secrev;
replace redsecrev0=0 if red_secrev==.;
g redsalrev0=red_salrev;
replace redsalrev0=0 if red_salrev==.;

g revindtrav=(redindrev0+redsecrev0+redsalrev0)*12; // je prends les revenus imputes
gen revindtrav_mis=(indrev==999998|indrev==999999|secrev==999998|secrev==999999|salrev==999998|salrev==999999);

bys hldid persid: g srevindtrav=revindtrav if _n==1; /* Je fais ca parce que les individus sont enquetes sur deux journees... */
bys hldid persid: g srevindtrav_mis=revindtrav_mis if _n==1;

*revenu du travail des individus du m�nage, total annuel;
bys hldid: egen sumrevindtrav=sum(srevindtrav);
bys hldid: egen sumrevindtrav_mis=sum(srevindtrav_mis);
qui g revind_decl=(srevindtrav>0 & srevindtrav!=.);
bys hldid: egen sumrevtrav=sum(revind_decl);
replace sumrevindtrav_mis=(sumrevindtrav_mis>0);
la var sumrevindtrav "Somme des revenus individuels du travail declares";
la var sumrevindtrav_mis "Info somme revenu ind trav manquante pr menage";

g oktrav=(sumrevtrav>=nbtravailleur & sumrevtrav!=.); /* variable indiquant si le nbr de revenus du travail collect� est au moins �gal au nombre d'actifs occupes, la difference provenant des retraites
etudiants etc. qui ont un revenu d'appoint */

g revmenhorstrav_mis=(revmen<sumrevindtrav|revmen==.|sumrevindtrav==.|sumrevindtrav_mis>0|oktrav==0);
g double revmenhorstrav=revmen-sumrevindtrav;
replace revmenhorstrav=0 if revmenhorstrav_mis==1;
la var revmenhorstrav "Unearned yearly household income; 0 if missing";
label var revmenhorstrav_mis "Unearned yearly household income missing";

g wagerate=((revindtrav/52)/workhours);
replace wagerate=0 if statemploi==4|statemploi==5|statemploi==6; 
/* Va falloir aussi virer les etudiants et les independants */
replace wagerate=wagerate*(107.8186/106.1942) if year==2009;
replace revmenhorstrav=revmenhorstrav*(107.8186/106.1942) if year==2009;

local varkeep "`varkeep'
			revindtrav revindtrav_mis sumrevindtrav sumrevindtrav_mis sumrevtrav oktrav revmenhorstrav revmenhorstrav_mis wagerate";

/* Imputations & Instruments */

g wage1=wagerate if statemploi==1|statemploi==2;
replace wage1=. if statemploi>2;
local varkeep "`varkeep' wage1";

	/* garde enfants */
	
	g aide1=0;
	g aide2=0;
	g nenf1=0;
	g nenf2=0;
	
	forv c=1/9	{;
		replace aide1=aide1+1 if (gardep`c'==3|gardep`c'==4);
		replace nenf1=nenf1+1 if (scol`c'==1|scol`c'==2);
		replace aide2=aide2+1 if (mercrem`c'==3|mercrem`c'==4)|(mercreg`c'==3|mercreg`c'==4);
		replace aide2=aide2+0.5 if (soirm`c'==3|soirm`c'==4)|(soirg`c'==3|soirg`c'==4);
		replace nenf2=nenf2+1 if (scol`c'==3);
				};
	qui g iaide1=aide1/nenf1 if nenf1!=0 & nenf1!=.;			
	qui g iaide2=aide2/(nenf2*1.5) if nenf2!=0 & nenf2!=.;
	qui g aideenf=((aide1>0 & aide1!=.)|(aide2>0 & aide2!=.));
	g aide3=(aid06==1);
	g aide3f=(aid06f>4 & aid06f!=.);
	
	la var iaide1 "intensite aide pour garde enfants non scol";
	la var iaide2 "intensite aide pour garde enfant scol";
	la var aide3 "a ete aide ds le mois par voisins ou parents pour garde enfant";
	la var aide3f "a ete aide plus de 4 fois ds le mois pour garde enfant";
	
	recode gardh .=0;
	g nochildcareexp=(gardh==0 & nenf1+nenf2>0 & nenf1!=. & nenf2!=.);
	g childcareexp=gardh;
	g childcareexpgeo1=(1-nochildcareexp)*rural;
	g childcareexpgeo2=(1-nochildcareexp)*(paris+urbain3);
	
	local varkeep "`varkeep' aide1 iaide1 aide2 iaide2 aideenf aide3 aide3f nochildcareexp childcareexp childcareexpgeo1 childcareexpgeo2 gardh";
	
	/* Caracteristiques emploi actuel: pour equation de salaire */
	
	g anciennete=0 if actoccup==2;
	replace anciennete=act1a+act1m/12 if actoccup==1;
	g anciennete2=anciennete^2;
	recode act2 (9=.) (2=1) (3=2) (4=2) (5=3) (6=3);
	qui ta act2, g(tailleentreprise);
	forv i=1/3	{;
		qui su tailleentreprise`i';
		qui replace tailleentreprise`i'=r(mean) if tailleentreprise`i'==. & actoccup==2;
				};
	local varkeep "`varkeep' anciennete anciennete2 tailleentreprise1-tailleentreprise3";
	
	/* Eloignement contraint de l'emploi: actifs ou chomeurs eloignes de l'emploi, eloignement contraint pour les anciens actifs */
	
	g ancienact=(actif==2 & statemploi!=3);
	for num 1/3: g eloignX=0;
	replace eloign1=(year-cess2<1 & year-cess2>0) if cess3==2 & ancienact==1;
	replace eloign2=(year-cess2<2 & year-cess2>=1) if cess3==2 & ancienact==1;
	replace eloign3=(year-cess2<3 & year-cess2>=2) if cess3==2 & ancienact==1;
	replace eloign1=1 if cho1>0 & cho1<4;
	replace eloign2=1 if cho1==4;
	replace eloign3=1 if cho1==5;
	
	local varkeep "`varkeep' ancienact eloign1 eloign2 eloign3";
	
	/* CSP des parents - notamment m�re au foyer */
	
	recode anaim (8=.) (9=.);
	g mereinactive=(fim==4|fim==5);
	g mereinactivemissing=(fim==9);
	replace mereinactive=0 if mereinactivemissing==1;
	
	g diffagemere=an_nais-anaim;
	g diffagepere=an_nais-anaip;
	replace diffagepere=. if anaip<10 ;// il ya des valeurs manquantes
	/*attention sur les diff�rences d'�ge il y a des valeurs aberrantes */;
	g merdipl0=(merdipl==1);
	g merdiplsup=(merdipl>2 & merdipl!=.);
	g perdipl0=(perdipl==1);
	g perdiplsup=(perdipl>2 & perdipl!=.);
	
	local varkeep "`varkeep' mereinactive diffagemere diffagepere merdipl0 merdiplsup perdipl0 perdiplsup";
	
	/* Variables croisees: cohortes*csprisk; revenu*structfam */
	
	 g cohort1=(an_nais<1945);
	 g cohort2=(an_nais>=1945 & an_nais<1960);
	 g cohort3=(an_nais>1960 & an_nais<1975);
	 g cohort4=(an_nais>1975);
	  
	 g csprisk1=inlist(cs42, 11, 12, 13, 21, 22);
	 g csprisk2=inlist(cs42, 23, 31, 35, 37, 38);
	 g csprisk3=inlist(cs42, 46, 47, 48); 
	 g csprisk4=(cs42>53 & cs42<70); /* NB: categorie de reference implicite = metiers proteges des fluctuations economiques */
	 
	 local i=1;
	 forv j=1/4	{;
	  	forv k=1/4	{;
	 	 	g interacteco`i'=csprisk`k'*cohort`j';
			local varkeep "`varkeep' interacteco`i'";
	 	 	local i=`i'+1;
	 	 			};
	 	 		};
	 local varkeep "`varkeep' csprisk1 csprisk2 csprisk3 csprisk4";
	 	 	
	 g struct1=(enf3ans>0 & nbe6am>0);
	 g struct2=(enf3ans==0 & nbe6am>0);
	 g struct3=(enf3ans==0 & nbe6am==0 & nbenfant>0);
	 g struct4=(enf3ans==0 & nbe6am==0 & nbenfant==0);	
	 qui su revmen, detail;
	 local i=1;
	 g seuil=0;
	 forv j=1/4		{;
	 	foreach v in 25 50 75 {;
	 		replace seuil=(revmen>r(p`v'));
	 		g interacttax`i'=struct`j'* seuil;
			local varkeep "`varkeep' interacttax`i'";
	 		local i=`i'+1;
	 	 						};
	 	 			};
	 local varkeep "`varkeep' struct1 struct2 struct3 struct4";
	

		
/* usages du temps & repas */

recode tem4 8=. 9=.;
rename tem4 sauterepas;
recode grignot 8=. 9=.;;
rename grignot snacking;

g takeaway=(serv32==1);
recode serv32p (2=52) (3=12) (4 =1), gen(freqr1);
g freqtakeaway=serv32f*freqr1;
drop serv32 serv32f serv32p freqr1;

g delivery=(serv33==1);
recode serv33p (2=52) (3=12) (4 =1), gen(freqr1);
g freqdelivery=serv33f*freqr1;
drop serv33 serv33f serv33p freqr;

local varkeep "`varkeep' takeaway freqtakeaway delivery freqdelivery";
 
keep `varkeep';
compress;
sort hldid persid day;
sa "${path}temp", replace;

/*****************/
/* Questions DCC */
/*****************/
/*
use "${path}qaaconjoints.dta", clear;

keep idind idmen scb6 plog plogval pimmo pbpro pepa passv pvmob pbiens patritr;
destring _all, replace;

rename idmen hldid;
rename idind persid;
rename patritr valpatrimoine;
g patrimoine=min(plog,.)+min(.,pimmo)+min(.,pbpro)+min(pepa,.)+min(passv,.)+min(pvmob,.)+min(pbiens,.);
g patrimoine0=(patrimoine==0);
g logement=(plog==1);
g logementval=0;
replace logementval=plogval if logement==1;

keep hldid persid patrimoine patrimoine0 logement logementval;
sort hldid persid;
sa "${path}tempdcc", replace;


use "${path}qaacouples.dta", clear;

keep idmen idind scbj6_r colog cologval coimmo cobpro coepa coass covmo;
destring _all, replace;

rename idmen hldid;
rename idind persid;
g copatrimoine=min(colog,.)+min(.,coimmo)+min(.,cobpro)+min(coepa,.)+min(coass,.)+min(covmo,.);
g copatrimoine0=(copatrimoine==0);
g cologement=(colog==1);
g cologementval=0;
replace cologementval=cologval if cologement==1;

keep hldid persid cologementval cologement copatrimoine copatrimoine0;
sort hldid;
merge 1:1 hldid persid using "${path}tempdcc";
keep if _merge==1|_merge==3;
drop _merge;
sort hldid;
sa "${path}tempdcc", replace;


use "${path}couples.dta", clear; 
keep idmen carfi carcom cafql cartout;
destring _all, replace;
rename idmen hldid;

g sharingrule=1 if carfi==1;
replace sharingrule=2 if carfi==2|carfi==3;
replace sharingrule=3 if carfi==4;
replace sharingrule=4 if carfi>4;

keep hldidi sharingrule;
sort hldid;
merge 1:m hldid using "${path}tempdcc";
sort hldid persid;
sa "${path}tempdcc", replace;
*/

/*******************/
/* emploi du temps */
/*******************/

use "${path}carnetdestr.dta", clear;
*destring _all, replace;
g day=1 if jour=="lundi";
replace day=2 if jour=="mardi";
replace day=3 if jour=="mercredi";
replace day=4 if jour=="jeudi";
replace day=5 if jour=="vendredi";
replace day=6 if jour=="samedi";
replace day=7 if jour=="dimanche";  

rename mois_carnet month;
rename annee_carnet year;
rename idind persid;
rename idmen hldid;

local varlist "repas repas141 repas142 repas143 repas144 repas145 repas146 repasprinc repasprincsssec repasprincsec repasprincvid repassec repassecvid 
				cuisine mealprep cuisineprinc mealprepprinc cuisineprincsec cuisineprincsssec cuisineprincvid cuisineprincseul cuisinesec cuisinesecvid
				repasseulprinc	repaspartprinc	repasfamprinc	repasdomprinc	repastraprinc	repasextprinc	repasdomseulprinc	
				repastraseulprinc	repasextseulprinc	repasdompartprinc	repastrapartprinc	repasextpartprinc	repasdomfamprinc	
				repastrafamprinc	repasextfamprinc	mealchores	travailsal	etude";
				

	local Lrepas141 "actpr\`t'==141|actse\`t'==141";
	local Lrepas142 "actpr\`t'==142|actse\`t'==142";
	local Lrepas143 "actpr\`t'==143|actse\`t'==143";
	local Lrepas144 "actpr\`t'==144|actse\`t'==144";
	local Lrepas145 "actpr\`t'==145|actse\`t'==145";
	local Lrepas146 "actpr\`t'==146|actse\`t'==146";

	local Lrepas "(actpr\`t'>140 & actpr\`t'<150)|(actse\`t'>140 & actse\`t'<150)";
	local Lrepasprincsssec "(actpr\`t'>140 & actpr\`t'<150) & actse\`t'==.";
	local Lrepasprincsec "(actpr\`t'>140 & actpr\`t'<150) & actse\`t'!=.";
	local Lrepasprincvid "(actpr\`t'>140 & actpr\`t'<150) & ((actse\`t'>630 & actse\`t'<640)|(actse\`t'>671 & actse\`t'<678))";
	local Lrepassec "(actse\`t'>140 & actse\`t'<150)";
	local Lrepassecvid "(actse\`t'>140 & actse\`t'<150)& ((actpr\`t'>630 & actpr\`t'<640)|(actpr\`t'>671 & actpr\`t'<678))";
	
	local Lcuisine "(actpr\`t'==311)|(actse\`t'==311)";
	local Lmealprep "(actpr\`t'>=311 & actpr\`t'<=313)|(actse\`t'>=311 & actse\`t'<=313)";
	local Lcuisineprinc "(actpr\`t'==311)";
	local Lmealprepprinc "(actpr\`t'>=311 & actpr\`t'<=313)";
	local Lcuisineprincsec "(actpr\`t'==311) & actse\`t'!=.";
	local Lcuisineprincsssec "(actpr\`t'==311) & actse\`t'==.";
	local Lcuisineprincvid "(actpr\`t'==311) & ((actse\`t'>630 & actse\`t'<640)|(actse\`t'>671 & actse\`t'<678))";
	local Lcuisineprincseul "(actpr\`t'==311) & pres\`t'==1";
	local Lcuisinesec "(actse\`t'==311)";
	local Lcuisinesecvid "(actse\`t'==311) & ((actpr\`t'>630 & actpr\`t'<640)|(actpr\`t'>671 & actpr\`t'<678))";
	
	
	local Lrepasprinc "(actpr\`t'>140 & actpr\`t'<150)";
	local Lrepasseulprinc "(actpr\`t'>140 & actpr\`t'<150 &  pres\`t'==1 )";
	local Lrepaspartprinc "(actpr\`t'>140 & actpr\`t'<150 &  pres\`t'!=1 )";
	local Lrepasfamprinc "actpr\`t'>140 & actpr\`t'<150 &  !inlist(pres\`t',1, 6, 66, 666, 6666, 66666)";
	local Lrepasdomprinc "(actpr\`t'==141 | actpr\`t'==144)";
	local Lrepastraprinc "(actpr\`t'==142 | actpr\`t'==145)";
	local Lrepasextprinc "(actpr\`t'==143 | actpr\`t'==146)";
	local Lrepasdomseulprinc "(actpr\`t'==141 & pres\`t'==1)";
	local Lrepastraseulprinc "actpr\`t'==142";
	local Lrepasextseulprinc "(actpr\`t'==143 & pres\`t'==1)";
	local Lrepasdompartprinc "(actpr\`t'==141 | actpr\`t'==144) & pres\`t'!=1";
	local Lrepastrapartprinc "actpr\`t'==145";
	local Lrepasextpartprinc "actpr\`t'==146 | (actpr\`t'==143 & pres\`t'!=1)";
	local Lrepasdomfamprinc "(actpr\`t'==141 & pres\`t'!=1) | (actpr\`t'==144 & !inlist(pres\`t',1, 6, 66, 666, 6666, 66666))";
	local Lrepastrafamprinc "(actpr\`t'==145 & !inlist(pres\`t'!=1, 6, 66, 666, 6666, 66666))";
	local Lrepasextfamprinc "(actpr\`t'==143 & pres\`t'!=1) | (actpr\`t'==146 & !inlist(pres\`t',1, 6, 66, 666, 6666, 66666))";
	local Lmealchores "actpr\`t'==312 | actpr\`t'==313";
	local Ltravailsal "((actpr\`t'>=211 & actpr\`t'<=234 ) | actpr\`t'==811 | actpr\`t'==142 | actpr\`t'==145)";
	local Letude "(actpr\`t'>=251 & actpr\`t'<=271)";
	

	g lagact=0;
	g cond=0;
	local i=1;
	local varcoll " ";
	foreach X in `varlist' {;
		qui g `X'=0;
		qui g n`X'=0;
		forv t=1/144	{;
			qui replace cond=(`L`X'');
			qui replace  `X'=`X'+10 if cond==1 ;
			qui replace n`X'=n`X'+1 if cond==1 & lagact==0;
			qui replace lagact=1 if cond==1 & lagact==0;
			qui replace lagact=0 if cond==0;
					};
		di "`i'";
			local ++i;			
		qui replace lagact=0;
		local varcoll "`varcoll' `X' n`X'";
			};

keep hldid persid `varcoll' day;
sort hldid persid day;
merge 1:1 hldid persid day using "${path}temp.dta";
keep if _merge==3;
cap drop _merge;
/*
sort hldid persid;
merge 1:1 hldid persid using "${path}tempdcc";
keep if _merge==1|_merge==3;
*/
compress;
sort hldid persid;
save "${path}EDT2010_France_janvier2013.dta", replace;
exit, clear;




