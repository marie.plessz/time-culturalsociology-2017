/*******************************************************
Projet : 	Time
auteur : 	Marie
date : 		06/04/2016

t�che: 
. recodage sur base fusionnee
*********************/
*  TMP_2_recode
use "$data/TMP_1_append", clear
* bnrepas
gen bnrepas=nrepasprinc
recode bnrepas(5/max=5)
label var bnrepas "N of meals (ppal)"
label def bnrepas 5 "5 or more", modify
label val bnrepas bnrepas

gen bnext=nrepasextprinc
recode bnext(3/max=3)
label var bnext "N of meals out (ppal)"
label def bnext 3 "3 or more", modify
label val bnext bnext

rename nhomemeals bndom

gen bndom0= bndom==0
gen bndom1= bndom==1
gen bndom2= bndom==2
gen bndom3= bndom==3
gen bndom4= bndom==4
replace bndom0=. if bndom==.
replace bndom1=. if bndom==.
replace bndom2=. if bndom==.
replace bndom3=. if bndom==.
replace bndom4=. if bndom==.

* part du cooking fait par main cook
gen maincook=cuisineprinc/sumcuisineprinc*100
gen seulcook=maincook==100
replace seulcook=. if sumcuisineprinc==0
label var seulcook "Only main cook cooked"
label var maincook "% cooking by maincook"

*interaction ann�e pays
egen yp=group(country enquete)
gen i_year2009= enquete==2009
gen i_US= country==1
gen i_2009US=  enquete==2009 &  country==1


egen ct=group(country enquete)
label def ct 1 "Fce-85" 2 "Fce-09" 3 "USA-85" 4 "USA-09", modify
label val ct ct
label var ct "Country-Year"
*codage variables indicatrices toujours dans le sens o� bonus attendu

gen nojob= worker
recode nojob (1=0) (0=1)
label var nojob "Main Ck no job"

* pstatus 0: no partner 1 : partner no job; 2: partner works
tab pstatus, gen(pstat)
clonevar  pnojob=pstat2
clonevar  pjob=pstat3
drop pstat1

recode single (0=1) (1=0), gen(couple)

*income reduced 
recode revclass (0 8000 10000=10000) (12000 14000 16000=16000)	///
	(18000 20000 24000=24000) (28000 32000 40000=40000) , gen(rev4cl)
label var rev4cl "Income class eur/y/cap"
tab rev4cl, gen(rev4cl)	

gen nokids=kids==0
gen nocoll=college==0
gen nowe=we==0
gen nocongel=congel==0
gen nolave=lavevaisselle==0	
* fin
compress
save "$data/TMP_2_recode", replace
label data "EDT Fce+US fusionnees recod�es"
