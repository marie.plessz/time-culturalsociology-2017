/*	Projet TIME
repris par marie le 24 juillet 2014
-	fusionne les bases Fce 1985 et 2010
- 	exclus les enfants et m�nages non standard
-	recode sitconj pour que les pers en couple qui se sont d�clar�es 
"c�lib/s�par�es" en 85 soient bien des couples
-	cr�e les vars en pvar pour les conjoints 
-	calcule sumcuisineprinc
-	conserve uniquement les femmes dans les couples, et uniqut les "maincook" �g�s de 18 � 63 ans.
-	calcule les poids dont la moyenne soit 1.

==> base des m�nages standards, 1 ligne par m�nage, temps de cuis total, variables indv et conjoint
�chantillon de travail.

==> attention par rapport � Fabrice 2013 je fais la s�lection sur l'�ge � un autre moment : je ne s�lectionne pas
des personnes de 18-64 ans mais des "main cook" de cet �ge-l�.

___V2 : pond�ration propwti : tient compte des jours de la semaine (poids carnet et non poids m�nage)

*/
version 11.0
cap log close
set more off

global path "$data"

/*	ANNEE 2010	*/
use "$path/EDT2009/EDT2010_France_janvier2013.dta", clear
cd "$path"
/*set logtype t
log using DecompEDT, replace	*/
di in b "This program was run at $S_TIME on $S_DATE"
set more off

	/*contr�les	*/
bys hldid: gen i=_n
label var i "num ligne dans hh"
bys hldid persid: gen c=_n
label var c "num carnet dans pers"
bys hldid c: gen p=_n
lab var p "num pers parmi les 1� carnets du hh"
tab p lienpref


/* Append the data sets */

u "$path/EDT1988/EDT85_France_janvier2013.dta", replace

cap drop aida*
cap drop aidb*
cap drop aidc*
cap drop aidd*
g enquete=0
append using "$path/EDT2009/EDT2010_France_janvier2013.dta"
ta enquete sex
replace enquete=1 if year>2000
replace hldid=hldid*10+enquete
sort hldid persid day

/*	exclusion des enfants	*/
keep if lienpref<2

/*	exclusion des m�nages de m�me sexe et des m�nages non standard	*/
tab enquete	sex 
drop if notstandardhhold==1
tab enquete	sex 

g samesex=1 if hldid==hldid[_n-1] & persid!=persid[_n-1] & sex==sex[_n-1] 
/* ajouter critere lienpref : inutile il est dans standardhhold1 */
bys hldid: egen ssamesex=sum(samesex)

/*	s�lection	*/
ta ssamesex enquete
sort hldid persid
ta enquete sex
drop if ssamesex>=1	/* vire les couples de m�me sexe	*/
ta enquete sex
keep if notstandardhhold ==0	/*vire les m�nages non standards */									
ta enquete sex											

/*recodages	*/

g age10=age/10

g catage=1 if age<25  
forv i=2/12	{
	qui replace catage=`i' if age>=25+5*(`i'-2) & age<25+5*(`i'-1)
				}
replace catage=13 if age>=80
	
rename zeat region
g nbinf=nbenfant
replace nbinf=4 if nbenfant>4 & nbenfant!=.
qui ta nbinf, g(nbinf)
	
g urbain0=(rural==1)
g urbain4=(paris==1)

foreach var in educa statemploi sitconj catage region {
		qui tabulate `var', nofreq g(`var')																				
														}

qui g worker=(statemploi==1|statemploi==2)																			

/*corriger sitconj car en 1985 ctn inds sont "seul/monop" mais avec un "conjoint" qui remplit carnet"	*/
bys hldid : egen r=sum(lienpref==1)
gen sitconjold=sitconj 
replace sitconj=1 if r==1 & sitconjold!=1
label var sitconj "Situation conjugale"
note sitconj: recod� si un conjoint appara�t dans m�nage et sitconj!= couple, attribu� la valeur couple (1).
drop r
save mp_append, replace

/* Match partner's characteristics */
save mp_temp, replace
	/*garder 1 ligne pour chaque pers en couple	*/
bys hldid persid : keep if _n==1 & sitconj==1 & (lienpref==0|lienpref==1)
g const=1
by hldid: egen nbind=sum(const)
ta nbind
	/*r�cup�rer l'identifiant du conjoint	*/
sort hldid persid
qui g double ppersid=persid[_n+1] if hldid==hldid[_n+1]
qui replace ppersid=persid[_n-1] if hldid==hldid[_n-1]

local varother "statemploi educa sexe worker age  cohort lienpref cuisineprinc"

	/*constituer un fichier des conjoints	*/
drop persid
rename ppersid persid
drop if persid==.
keep hldid persid nbind `varother' 
foreach var of local varother	{
	rename `var' p`var'
								}
sort hldid persid
sa "$path/partnertemp", replace

u "$path/mp_temp", clear
merge m:1 hldid persid using "$path/partnertemp"
keep if _merge==1|_merge==3
ta sitconj _merge
g matchpartnernonmissing=(_merge==3 & sitconj==1 & (lienpref+plienpref==1))
cap drop _merge

replace nbind=1 if nbind==.
sort hldid persid
ta enquete sex
sa "$path/mp_append", replace

u "$path/mp_append", clear

g empstat=statemploi
replace empstat=3 if statemploi>2 & statemploi<.
g pempstat=pstatemploi
replace pempstat=3 if pstatemploi>2 & pstatemploi<.


foreach var in peduca pstatemploi empstat pempstat	{
		qui tabulate `var', nofreq g(`var')																				
									}
g diffage10=age10-page/10
replace diffage10=max(-2, min(diffage10,2))
label var diffage10 "Age diffce bwn spouses"

local varpartner "diffage10 peduca1 peduca2 peduca3 peduca4 pworker pcuisineprinc"

foreach var of local varpartner	{
	qui g `var'_mis=((sitconj==1 & matchpartnernonmissing==0)|(sitconj==1 & matchpartnernonmissing==1 & `var'==.))
	qui replace `var'=0 if `var'_mis==1|sitconj!=1
								}

replace revmenhorstrav=0 if revmenhorstrav<0
replace revmenhorstrav_mis=1 if revmenhorstrav<0
replace revmenhorstrav=0 if revmenhorstrav_mis==1

/*	Temps cuisine couple	*/
gen sumcuisineprinc=cuisineprinc + pcuisineprinc
label var sumcuisineprinc "Cooking time total HH"

save "$path/mp_append"	, replace

/*	vars de contr�le*/
bys hldid: gen i=_n
label var i "num ligne dans hh"
bys hldid persid: gen c=_n
label var c "num carnet dans pers"
bys hldid c: gen p=_n
lab var p "num pers parmi les 1� carnets du hh"
tab p lienpref

/*	clean the sample	*/
ta enq	sitconj
ta enq	sexe

/*	exclure les couples o� un seul r�pondant & les m�nages dont sitconj pas coh�rent ac nb adultes	*/
bys hldid day: gen n=_N

ta sitconj enquete if c==1
drop if n==1 & sitconj==1
ta sitconj enquete if c==1
drop if n==2 & sitconj!=1
ta sitconj enquete if c==1
/*	conserver seulement la femme si couple	*/
label var sex "Male?"
label def sex 0"Female" 1 "Male"
label val sex sex
bys hldid day (sex) : keep if _n==1
ta sitconj enquete if c==1
/* conserver seulement les m�nages o� pers de r�f est �g�e de 18 � 63 ans	*/
keep if age<64 & age>18
ta sitconj enquete if c==1
drop i c p n nbind
compress


	/* calcul des poids de redressement a partir de propwti */
cap drop poidsredressement1 poidsredressement2
levelsof enquete, local(levels)
macro list _levels

*si traitements s�par�s selon sexe ;
qui g double poidsredressement1=.
foreach enq of local levels {
di "`enq'"
	}
	
foreach enq of local levels	{
	forv s=0/1		{
			qui su propwti if sex==`s' & enquete==`enq'
			 qui replace  poidsredressement1=propwti/(r(N)*r(mean)) if sex==`s' & enquete==`enq'
					}
				}
label var poidsredressement1 "Poids par sexe" 

*si traitement sans distinction de sexe ;
qui g double poidsredressement2=. 
foreach enq of local levels	{
	qui su propwti if  enquete==`enq'
	qui replace  poidsredressement2=propwti/(r(N)*r(mean)) if  enquete==`enq'
	}
	
label var poidsredressement2 "Poids par m�nage" 	
ta enquete sexe	[aw=poidsredressement2]
	
label var poidsredressement2 "Poids par m�nage" 				

recode enquete 0=1985 1=2009
compress
save "$path/mp_Fce_3_couple", replace
