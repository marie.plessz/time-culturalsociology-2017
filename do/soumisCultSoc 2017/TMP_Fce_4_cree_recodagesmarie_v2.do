/*	Projet : TIME
Marie le 24 juillet
cr�e les vars que je vais utiliser dans les d�compositions

*/
set more off
global path "$data"
use "$path/mp_Fce_3_couple", clear
/*********************************************************************
***	Recodages adapt�s aux faibles effectifs de l'enqu�te USA1985
*********************************************************************/
/*	revenu du m�nage topcod�	*/
gen revpc=revmen/sqrt(nbadulte+nbenfant)
replace revpc=100000 if revpc>100000 & revpc<.

label var revpc "Revenu annuel percap euro2010"
note revpc: topcod� � 100000 euros/an.


cap drop revclass*
gen revclass=.
local class " 0 8000 10000 12000 14000 16000 18000 20000  24000  28000   32000 40000    "
foreach c in `class'	{
	replace revclass=`c' if revpc>`c' & revmen<.
	}
label var revclass "Percap. yearly income (euros2010)"
 tab revclass, gen(revclass)
 
/*cap drop logclas
gen logclas=.
local class "8 9 9.3 9.6 9.9 10.2 10.5 10.8 11 "
di "`class'"
foreach c in `clas'	{
	replace logclas=`c' if logrevmen>`c' & revmen<.
	}
*/
	# delimit ;
*Structure des m�nages	;
			/*pour les stats desc	*/
gen pstatus=0	;
replace pstatus=1 if pworker==0 & sitconj==1	;
replace pstatus=2 if pworker==1		;
tab pstatus	;
label var pstatus "Partner status"	;
label def pstatus 0"No partner" 1 "Partner no job" 2 "Partner works"	;
label val pstatus pstatus		;
		/* pour les mod�les	*/
gen single=	sitconj!=1	;
label var single "Single-adult HH"	;
label def single 0 "Couple" 1 "Single", modify	;
label val single single	;

gen kids=	nbenfant !=0	;
label var kids "Children in HH"	;
label def kids 0 "No kids" 1 "1+ kid in HH", modify	;
label val kids kids	;

* niveau d'�tude dichotomique: college ou pas	;
gen college= 	educa==3| educa==4	;
gen pcollege=	peduca==3| peduca==4	;
label def college 0 "No college" 1 "College", modify	;
label def pcollege 0 "No partner/partner no college" 1 "Partner: college", modify	;
label val college college	;
label val pcollege pcollege	;
label var college "Resp education college"	;
label var pcollege "Partner education"	;

*age :tranches de 10 ans	;
recode age (min/29= 20) (30/39=30)(40/49=40)(50/64=50)		, gen(aged)	;
label var aged "Age class"	;
tab aged, gen(aged)	;
format aged* %2.0f	;
 
*	ou groupe d'age	;
cap drop agegr ;
recode age  (0/34=34) (35/59=59)(60/max=60) (-8=.) ,gen(agegr)	;
 label def agegr 34 "18-34y" 59 "35-49y" 60 "50-64y", modify	;
 label val agegr agegr	;
 label var agegr "Age group"	;
 format agegr %2.0f	;
 tab agegr, gen(agegr)	;
 
 
 *we ;
 recode day (1/5=0)(6 7=1), gen(we);
 label def we 0 "Weekday" 1 "Week-end";
 label val we we;
 label var we "Week-end?";
 
 bys hldid persid (day) : gen carday=_n ;
 
 *	borner sumcuisineprinc	;
 replace sumcuisineprinc=300 if sumcuisineprinc>400 & sumcuisineprinc<.	;
 note sumcuisineprinc: born� � 300 min	;

 
/*	Temps de cuisine non-nul	*/
gen somecooking=sumcuisineprinc>0	;
replace somecooking=. if sumcuisineprinc==.	;
label var somecooking "Any cooking (HH)?"	;
label def somecooking 0 "No cooking in HH" 1 "Some cooking in HH"	, modify	;
label val somecooking somecooking	;
 
 * labels manquants ;
 label def worker 0 "Resp. no job" 1 "Resp. works", modify;
 label val worker worker;
 label var worker "Main cook works?";
 
 label var sumcuisineprinc "HH total cooking time" ;
 
 
	* nombres repas � domicile ;
recode nrepasdomprinc (4 /max=4 "4 and more"), gen(nhomemeals) ;
label var nhomemeals "N of meals at home (Main Ck)" ;

tab nhomemeals, gen(homem ) ;
rename (homem*) (homem0 homem1 homem2 homem3 homem4) ;

g old1985=(enquete==1985);

 *du nettoyage	;
 
drop   nrepassecseul repassec nrepassec repassecvid nrepassecvid repastraprinc 
	nrepastraprinc repasdomprinc  repasresprinc nrepasresprinc repasautprinc
	nrepasautprinc repaspartprinc nrepaspartprinc repasdompartprinc nrepasdompartprinc
	repastrapartprinc nrepastrapartprinc repasextpartprinc nrepasextpartprinc 
	repasdomseulprinc nrepasdomseulprinc repastraseulprinc nrepastraseulprinc 
	repasextseulprinc
	nrepasextseulprinc repasdomfamprinc nrepasdomfamprinc repastrafamprinc nrepastrafamprinc
	repasextfamprinc nrepasextfamprinc repasfamprinc nrepasfamprinc repasextprinc
	repas141 nrepas141 repas142 nrepas142 repas143 nrepas143 repas144
	nrepas144 repas145 nrepas145 repas146 nrepas146
	repastra nrepastra repasdom nrepasdom repasres nrepasres repasaut nrepasaut 
	repasprincsssec nrepasprincsssec repasprincsec nrepasprincsec repasprincvid 
	nrepasprincvid repasseulprinc nrepasseulprinc repassecseul
	cuisineprincsec ncuisineprincsec cuisineprincsssec ncuisineprincsssec cuisineprincvid
	ncuisineprincvid cuisineprincseul ncuisineprincseul cuisinesec ncuisinesec 
	cuisinesecvid ncuisinesecvid 
	;

 
 compress ;
 save "$path/mp_Fce_4_Estsample.dta", replace ;

