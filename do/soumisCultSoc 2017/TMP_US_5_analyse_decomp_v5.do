/*********************************************
Projet	TIME
auteur	MP
date	23juillet2014
taches	
	recodages adapt�s pour les USA car petits effectifs en 85
	statistiques descriptives
	d�composition oaxaca-blinder 85-2010 sur moyenne et mediane
	
*********************************************/

use  "$data/USAestsample_hld.dta", clear 
set more off

# delimit ;
local varint "aged college worker pstatus kids  urban2  we nhomemeals "	;
misstable sum `varint', gen(m_)	;


tabout  `varint' enquete using "$res/mp_US_samplecar.txt",
	rep c(freq col)   f(0c 1c) show(none) h1(Sample caracteristics) h3(Unweighted distribution);
	tabout  `varint'  enquete [aw=poidsredressement2] using "$res/mp_US_samplecar.txt",
	append c(col)   f(1c) show(none) h1(Sample caracteristics) h3(Weighted distribution);
tabout `varint'  enquete   [aw=poidsredressement2] using "$res/mp_US_samplecar.txt",
	c(mean sumcuisineprinc ) f(1c ) sum append  h1(HH total cooking time) h3( Weighted mean )  show(none) ;
tabout  `varint'  enquete  [aw=poidsredressement2] using "$res/mp_US_samplecar.txt",
	c(median sumcuisineprinc ) f(0c ) sum append h1(HH total cooking time) h3( Weighted median) show(none) ;	
	
	

/*	descriptive statistics	*/

aorder;
/*
local varexp "age  aged1  aged2  aged3  aged4  
				college worker 
				single kids
				page   pworker 
			    urban2 regionus1-regionus4
			";
set more off ;
	bys  enquete: su sumcuisineprinc `varexp';
	bys  enquete: su sumcuisineprinc `varexp' [iweight=poidsredressement2]; 
*	bys enquete: su sumcuisineprinc `varexp' if missing2==0 [iweight=poidsredressement2]; 
*/	
*************************************
*	DECOMPOSITION cuisine m�nage	*
*************************************

# delimit ;								
	/* List Variables */									
													
local varcont "diffage10  urban2 " ; 
di  "`varcont'"	;	
local i=1;
local control "";
foreach var of local varcont	{;
	cap drop varcont`i';
	qui g varcont`i'=`var';
	local control "`control' varcont`i'";
	local i=`i'+1;
								}; 
di  "`control'";
aorder;

local varexp "	age: aged1  aged2  aged3  aged4  ,
				controls: `control'" ;				
local varexpreg "  aged2  aged3  aged4  
				college worker   
				   single kids
				 pworker  c.diffage  we";
local varexp2 "   aged2  aged3  aged4  
				college worker   
				   single kids 
				 pworker  we `control'";
	

local allvardep "sumcuisineprinc";
di  "`control'";
di  "`allvardep'";
di  "`varexp'";
di  "`varexp2'";

****************************************
*	OAXACA du 18 juillet : syntaxe diff�rente de 2013 je sais pas pourquoi
j'ai du enlever l'option "relax"
*****************************************


*keep if we==0;
*keep if we==1;
# delimit ;	
	/*  1	R�gression avec des pond�rations pour tenir compte des diff�rences d'effectifs entre les 2 enqu�tes	*/
	 
foreach vardep of local allvardep	{;
	/* Mean Decomposition  1985-2009 */
	
	reg `vardep' `varexp2' if enquete==1985 [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", replace bdec(3) ctitle("Mean-1985");
	
	reg `vardep' `varexp2' if enquete==2009 [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", append bdec(3) ctitle("Mean-2009");
	
	quietly oaxaca `vardep' `varexp2' [pweight=poidsredressement2], 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
	outreg2 using "$res/mp_US_decomp", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");
	
	/* Somecooking Decomposition  1985-2009 */
	
	logit somecooking `varexp2' if enquete==1985 [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", append bdec(3) ctitle("Participation-1985");
	
	logit somecooking `varexp2' if enquete==2009 [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", append bdec(3) ctitle("Participation-2009");
	
	quietly oaxaca somecooking `varexp2' [pweight=poidsredressement2]	, 
		by(old1985) weight(1) model1(logit, robust) model2(logit, robust) noisily /*relax*/
		detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
	outreg2 using "$res/mp_US_decomp", append onecol noobs bdec(3) ctitle("Participation 1985-2009");

	/* conditional mean */
	reg `vardep' `varexp2' if enquete==1985 & somecooking==1 [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", append bdec(3) ctitle("Cond. Mean-1985");
	
	reg `vardep' `varexp2' if enquete==2009 & somecooking==1  [pweight=poidsredressement2], robust;
	outreg2 using "$res/mp_US_regress", append bdec(3) ctitle("Cond. Mean-2009");
	
	quietly oaxaca `vardep' `varexp2' if somecooking==1  [pweight=poidsredressement2], 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
	outreg2 using "$res/mp_US_decomp", append onecol noobs bdec(3) ctitle("Conditional Mean 1985-2009");
};

exit;	
