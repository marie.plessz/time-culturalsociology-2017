/********************
Projet : 	Time
auteur : 	Marie
date : 		11/12/2017

tâche: sélectionner sur âge
V1 : révision cultural sociology
*********************/

/* selon enquête, age min et max varie */

use  "$data/TMP_2.2_recode", clear

count
* ==>   33,372

table ct, c(min age max age freq)
/*
----------------------------------------------
Country-Y |
ear       |   min(age)    max(age)       Freq.
----------+-----------------------------------
   Fce-85 |         17          98       7,740
   Fce-09 |         17         103      15,495
   USA-85 |         19          80         997
   USA-09 |         19          85       9,140
--
*/

*Select population of similar age

drop if age<19
drop if age>80

count 
table ct, c(min age max age freq)
table ct
/*
count :  32,276

----------------------------------------------
Country-Y |
ear       |   min(age)    max(age)       Freq.
----------+-----------------------------------
   Fce-85 |         19          80       7,552
   Fce-09 |         19          80      14,598
   USA-85 |         19          80         997
   USA-09 |         19          80       9,129
----------------------------------------------

----------------------
Country-Y |
ear       |      Freq.
----------+-----------
   Fce-85 |      7,552
   Fce-09 |     14,598
   USA-85 |        997
   USA-09 |      9,129
----------------------
*/


misstable sum bndom  couple kids nojob college 	aged we sumcuisineprinc, all

drop if sumcuisineprinc==.
* (206 observations deleted)


count
table ct

/* ==>  32,070

----------------------
Country-Y |
ear       |      Freq.
----------+-----------
   Fce-85 |      7,552
   Fce-09 |     14,598
   USA-85 |        967
   USA-09 |      8,953
----------------------
*/

* ==> "population agee de 19 à 80 ans inclus"

save "$data/TMP_3.2_sample", replace
