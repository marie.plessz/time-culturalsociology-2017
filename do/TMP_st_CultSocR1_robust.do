* TIME Soccult R1 le 16/01/2018
* MP

* test de robustesse : dans quelle mesure changement de la taille des épisodes pourrait expliquer déclin cuisine en Fce?

* 1: récupérer échantillon de travail du fichier de travail

use "$data/TMP_3.2_sample", clear


keep if country == 0 & enquete==1985

count
keep persid hldid
replace hldid=hldid/10
tostring hldid, gen(im)
sort im
save "$temp/t_sample_Fce85", replace


* sélectionner le même échantillon sur les données de base carnet
use "$data\EDT1988\carnetdestr.dta", clear

rename nquest hldid


gen double persid=hldid*100+nib2
tostring hldid, gen(im)
sort im
merge m:m  im using  "$temp/t_sample_Fce85"

/*
    Result                           # of obs.
    -----------------------------------------
    not matched                       257,562
        from master                   257,562  (_merge==1)
        from using                          0  (_merge==2)

    matched                           277,686  (_merge==3)
    -----------------------------------------
*/
keep if _merge==3

save "$temp/t_robust_Fce85", replace
* nb d'épisodes de cuisines et nb dépisodes de cuisine de 5 min

count if (primb2==311|primb2==312)
local d1=`r(N)'
count if (primb2==311|primb2==312) & duree<=5
local d2=`r(N)'

di " épisodes <= 5 minutes: `d2' / `d1' =" %6.2f `d2' / `d1' *100 "%"

* durée totale cuisine individuelle: * pb dans ma base de départ je n'ai que les identifiants des principaux cuisiners
g X=0
gen X5=0
replace  X=duree if (primb2==311|primb2==312)
replace  X5=duree if (primb2==311|primb2==312) & duree>5
sort im
collapse (sum) X X5, by(im )

sum X X5

exit



local Lcuisineprinc "(primb2==311|primb2==312)"
