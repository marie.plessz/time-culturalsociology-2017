cap log close 
log using "$res/TMP_st_R1CultSoc_controlcodage_log", text replace
* MP le 15/12/2018
* comparer rézultats selon codage bndom : continu ou pas.

set more off
use  "$data/TMP_3.2_sample", clear
tab aged yp

*** effet du codage continu ou pas de bndom
*==> n'affecte pas les résultats.
local varexpi " bndom1-bndom4  couple kids nojob college 	aged2 aged3 aged4 aged5 we"
local varexp " bndom  couple kids nojob college 	aged2 aged3 aged4 aged5 we"
forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	quietly reg sumcuisineprinc `varexpi'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo mi`ct'
		}
esttab m1 mi1 m2 mi2  , not r2 wide 	b(%6.2f) title(Temps de cuisine: comparaison codage bndom) 
esttab  m3 mi3 m4	 mi4 , not r2 wide 	b(%6.2f) title(Temps de cuisine: comparaison codage bndom) 


	

		************** OAXACA comparisons: ****************************
		*   /!\ population:  18-80

		
		*	#1	: continuous bndom, bndom + control ;
	# delimit ;
local varexp2 " bndom  (controls: single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";	
local cat "aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", replace  ctitle("USA_detcont") 
	side noparen onecol  nose  noobs bdec(2) excel ;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", append   ctitle("France_detcont") 
	side noparen onecol  nose noobs bdec(2) excel;		

*	#2	: continuous bndom, pooled effects ;
	# delimit ;
local varexp2 " (Total: bndom   single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";	
local cat "aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", append  ctitle("USA_poolcont") 
	side noparen onecol  nose  noobs bdec(2) excel ;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", append  ctitle("France_poolcont") 
	side noparen onecol  nose noobs bdec(2) excel ;		
	
**** #3 comme dans version soumise: bndom discret, pooled effects ;
local varexp3 " (Total: bndom0-bndom4 single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";
local cat "bndom0-bndom4, aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;

*USA ;
oaxaca sumcuisineprinc `varexp3' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", append  ctitle("USA_old") 
	  noparen onecol  nose   noobs bdec(2) excel;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp3' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_controlcodage", append  ctitle("France_old") 
	 noparen onecol  nose   noobs bdec(2)  excel ;		

	  *fin ;
	  cap log close;
	  exit;
