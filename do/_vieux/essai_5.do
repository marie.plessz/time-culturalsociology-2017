cap log close
set linesize 85
log using "$res/essai_5", text replace
set more off
use  "$data/TMP_2_recode", clear

* seult pr�sence ou non du conjoint

/*	Le temps de cuisine a-t-il d�clin� � l'�chelle du m�nage?	*/	
******************************************************************

* temps cuisine et nb repas dom
table enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking mean bndom mean bnrepas)  format (%6.2f)
table bndom enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking)  format (%6.2f)
* tps cuisine et �quipement (Fce)
table enquete if country==0 [pw=poidsredressement2], c(mean lavevaisselle mean congel mean microonde)  format (%6.2f)
corr lavevaisselle congel microonde if country==0
table enquete congel [pw=poidsredressement2] if country==0	 ///
	, c(mean microonde mean somecooking mean sumcuisineprinc mean bndom)  format (%6.2f)

* tps cuisine et cposition m�nage
table enquete country [pw=poidsredressement2], c(mean single mean kids mean worker mean pworker)  format (%6.2f)

table couple enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking  mean bndom mean congel)  format (%6.2f)

*contribution du main cook dans les couples
table country enquete if couple==1 & sumcuisineprinc!=0 , c( mean seulcook mean maincook n seulcook )  format (%6.2f)

 

/*	Quelles sont les caract�ristiques des m�nages associ�es au d�clin dans les 2 pays?	*/
* toutes les variables sont cod�es de fa�on � faire appara�tre ce qui fait "plus cuisiner"
local varexp " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 	aged2 aged3 aged4 we"
local eq "congel lavevaisselle"
local inc "rev4cl1 rev4cl2 rev4cl3"

forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc `varexp' `eq' `inc' [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 , not r2  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: comparaison)
	esttab meq1 meq2 , not r2  mtitle("Fce-85" "Fce-09") 	///
		title(Temps de cuisine: France full model )
forvalues ct=1/4	{
	quietly reg cuisineprinc `varexp' 	///
		[pw=poidsredressement2] if ct==`ct' & sexe==0, robust
	eststo mck`ct'
	}	
esttab mck1 mck2 mck3 mck4 , not r2  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: femmes )
cap log close
	
/*	D�composons les d�clins pour confirmer	*/	
	*	==> oaxaca
* USA
# delimit ;
local varexp "	age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4, couple: couple single
	, kids:  kids nokids, worker: worker nojob, educ:college nocoll, we: we nowe" ;				
local varexp2 " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4, 
	bndom0 bndom1 bndom2 bndom3 bndom4
	, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
quietly  oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_5_oaxaca", replace onecol noobs bdec(3) ctitle("USA")
	nose ;


* France	;
# delimit ;
local varexp "	age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4, couple: couple single
	, kids:  kids nokids, worker: worker nojob, educ:college nocoll, we: we nowe" ;				
local varexp2 " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4, 
	bndom0 bndom1 bndom2 bndom3 bndom4
	, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_5_oaxaca", append onecol noobs bdec(3) ctitle("France") nose;

* Frence : full model ;
# delimit ;
local varexp "	age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4, couple: couple single
	, kids:  kids nokids, worker: worker nojob, educ:college nocoll
	, we: we nowe, income: rev4cl1 rev4cl2 rev4cl3 rev4cl4
	, congel: congel nocongel, lavevaisselle : lavevaisselle nolave" ;					
local varexp2 " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 
	aged2 aged3 aged4 we congel lavevaisselle rev4cl1 rev4cl2 rev4cl3";
local cat "aged1 aged2 aged3 aged4, bndom0 bndom1 bndom2 bndom3 bndom4
	, single couple, kids nokids, worker nojob, college nocoll, we nowe
	, rev4cl1 rev4cl2 rev4cl3 rev4cl4, congel nocongel, lavevaisselle nolave" ;

	quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_5_oaxaca", append onecol noobs bdec(3) ctitle("France-full") nose;

* France-USA 2009	;
# delimit ;
local varexp "	age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4, couple: couple single
	, kids:  kids nokids, worker: worker nojob, educ:college nocoll, we: we nowe" ;				
local varexp2 " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4, 
	bndom0 bndom1 bndom2 bndom3 bndom4
	, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if enquete==1985, 
				by(country) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_5_oaxaca", append onecol noobs bdec(3) ctitle("1985") nose;	
quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if enquete==2009, 
				by(country) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_5_oaxaca", append onecol noobs bdec(3) ctitle("2009") nose;


exit;
