cap log close
set linesize 85
log using "$res/TMP_st_R1CultSoc_V2", text replace

/* R1 : isoler effet de bndom ds les résultats ET traiter bndom comme une var continue... 
toutefois j'ai traité comme continue une var bornée à 4 (150 cas >4 aux USA en 
cumulant les 2 enquetes).
*/

/* /!\ le package oaxaca a été mis à jour. j'utilise ici l'ancienne version.
sur un ordi où mise à jour a été faite (adoupdate) remplacer oaxaca par oaxaca9.
dans ma version l'option pour gérer les vars catégorielles s'appelle categorical
dans la nouvelle elle s'appelle normalize et s'utilise différemment.
*/

* setup
set more off
use  "$data/TMP_3.2_sample", clear
svyset [pw=poidsredressement2]


label def yp 1 "Fce 85" 2 "Fce 10" 3 "US 85" 4 "US 10", modify
label var yp yp

gen condcooking=sumcuisineprinc if somecooking==1


recode bndom(5=4 "4+"), gen(bn2)

 
	
******TABLE 1

/*	Le temps de cuisine a-t-il décliné à l'échelle du ménage?	*/	
******************************************************************



cap erase "$res/R1CultSoc_desc_outreg.txt"
cap erase "$res/R1CultSoc_desc_outreg.csv"
foreach var of varlist sumcuisineprinc  somecooking condcooking bndom bnext couple kids nojob college aged1	aged2 aged3 aged4 aged5 we {
	quietly svy : regress `var' ibn.ct, nocons	 
	outreg2 using "$res/R1CultSoc_desc_outreg",  stats(coef ci) label dec(1) noaster nor2 noobs nodepvar	excel
}	
	
tab yp
	
***** FIGURE
*association bivariée temps de cuisine nb repas.
svy: regress sumcuisineprinc i.bn2#i.enquete#i.country
margins i.bn2#i.enquete#i.country
marginsplot , plotdimension(enquete)  ///
	byopt(title("Mean cooking times according to number of eating episodes" "country and year", size(small)) ///
	note("Household cooking times and number of eating episodes by main cook, TUS, MP 19 12 2017") ///
	) 	legend ( order(1 "1985" 2 "2010")) ytitle("Minutes (95% CI)", size(small)) ///
	xtitle(Number of eating episodes/day, size(small)) saving("$res/R1CultSoc_Fig_bivariate", replace)
	
	
************* TABLE 3
/*	L'association entre nombre de repas et durée de la cuisine a-t-elle changé dans les 2 pays?
* toutes les variables sont codées de façon à faire apparaître ce qui fait "plus cuisiner"
local varexp " bndom  couple kids nojob college 	aged2 aged3 aged4 aged5 we"
local varexpi " bndom1-bndom4  couple kids nojob college 	aged2 aged3 aged4 aged5 we"
local eq "congel lavevaisselle"
local inc "rev4cl1 rev4cl2 rev4cl3"
cap drop predct*
forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	predict  predct`ct'	// prédiction pour tous les individus ac les 4 régressions
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc `varexp' `eq' `inc' [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 , not r2  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: comparaison)
	esttab meq1 meq2 , not r2  mtitle("Fce-85" "Fce-09") 	///
		title(Temps de cuisine: France full model )
	esttab  m3 m4 m1 m2 meq1 meq2 , not r2  mtitle("USA-85" "USA-09" "Fce-85" "Fce-09" "Fce-85" "Fce-09" ) ///
		title(Temps de cuisine: comparaison) 

	esttab  m3 m4 m1 m2 , not r2 ci wide  mtitle("USA-85" "USA-09" "Fce-85" "Fce-09")  ///
		b(%6.2f) title(Temps de cuisine: comparaison ci) 		
		
forvalues ct=1/4	{
	quietly reg cuisineprinc `varexp' 	///
		[pw=poidsredressement2] if ct==`ct' & sexe==0, robust
	eststo mck`ct'
	}	
esttab mck1 mck2 mck3 mck4 , not r2 p wide mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: femmes )
cap log close

********** OAXACA *****************

	# delimit ;
local varexp2 " bndom  (controls: single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";	
local cat "aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;	
	
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", replace  noobs bdec(2) ctitle("USA") 
	side noparen onecol  nose /* pour pouvoir calculer les ci */;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", append  noobs bdec(2) ctitle("France") 
	side noparen onecol  nose /* pour pouvoir calculer les ci */;		
	
**** comme dans version soumise: pas de détails dans chaque effet. 
*==> coder bndom continu ou catégoriel ne change pas fondamentalt les résultats
local varexp3 " (Total: bndom0-bndom4 single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 aged5 we nowe)";
local cat "bndom0-bndom4, aged1 aged2 aged3 aged4 aged5 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;

*USA ;
oaxaca sumcuisineprinc `varexp3' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", append  noobs bdec(2) ctitle("USA_pool") 
	  onecol  side ci /* pour pouvoir calculer les ci */;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp3' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/R1CultSoc_oaxaca", append  noobs bdec(2) ctitle("France_pool") 
	  onecol side ci  /* pour pouvoir calculer les ci */;		
