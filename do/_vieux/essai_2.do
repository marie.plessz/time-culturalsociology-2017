*essai_comparaison
*date : 11/04/2016


* temps cuisine et nb repas dom
* tps cuisine et �quipement (Fce)
* tps cuisine et cposition m�nage

use  "$data/TMP_2_recode", clear

egen ct=group(country enquete)
label def ct 1 "Fce-85" 2 "Fce-09" 3 "USA-85" 4 "USA-09", modify
label val ct ct

/*	Statistiques descriptives	*/

* temps cuisine et nb repas dom
table enquete country, c(mean sumcuisineprinc mean somecooking mean bndom mean bnrepas)
table bndom country enquete, c(mean sumcuisineprinc mean somecooking)
* tps cuisine et �quipement (Fce)
table enquete if country==0, c(mean lavevaisselle mean congel mean microonde)
corr lavevaisselle congel microonde if country==0
table enquete congel if country==0	///
	, c(mean microonde mean somecooking mean sumcuisineprinc mean bndom)

* tps cuisine et cposition m�nage
table enquete country, c(mean single mean kids mean worker mean pworker)

table kids enquete country, c(mean somecooking mean sumcuisineprinc mean bndom mean congel)



/*	mod�les	*/
* temps repas et nb repas dom
reg sumcuisineprinc i_* i.bndom we [pw=poidsredressement2]

* tps cuisine et �quipement (Fce)

reg sumcuisineprinc i.enquete we [pw=poidsredressement2] if country==0
reg sumcuisineprinc i.enquete  congel lavevaisselle we [pw=poidsredressement2] if country==0
reg sumcuisineprinc i.enquete##(congel lavevaisselle) i.bndom  we   [pw=poidsredressement2] if country==0

logit congel i.enquete single kids worker pworker  we [pw=poidsredressement2] if country==0

* tps cuisine et cposition m�nage
reg bndom i_* single kids worker pworker we [pw=poidsredressement2]
*	reg bnrepas i_* single kids worker pworker

reg sumcuisineprinc i_* i.bndom  we [pw=poidsredressement2]
reg sumcuisineprinc i_* i.bndom single kids worker pworker we [pw=poidsredressement2]
bys country: reg sumcuisineprinc i.enquete i.bndom single kids worker pworker we [pw=poidsredressement2]
reg sumcuisineprinc i.enquete##congel i.bndom  single kids worker pworker we  if country==0  [pw=poidsredressement2]

/*	oaxaca	*/
* USA
# delimit ;
local varexp "	age: aged1  aged2  aged3  aged4	
	, homemeals:  homem1 homem2 homem3 homem4" ;				

local varexp2 "  homem1 homem2 homem3 homem4 aged2  aged3  aged4  
				 college     worker single      kids    pworker 
				 we ";

 oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
*	outreg2 using "$res/mp_Fce_decomp", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");


* France
# delimit ;
local varexp "	age: aged1  aged2  aged3  aged4	
	, homemeals:  homem1 homem2 homem3 homem4" ;				

local varexp2 "  homem1 homem2 homem3 homem4 aged2  aged3  aged4  
				 college     worker single      kids    pworker 
				 we congel";

 oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
*	outreg2 using "$res/mp_Fce_decomp", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");


/*	OAXACA conditional	*/
* USA
# delimit ;
local varexp "	age: aged1  aged2  aged3  aged4	
	, homemeals:  homem1 homem2 homem3 homem4" ;				

local varexp2 "  homem1 homem2 homem3 homem4 aged2  aged3  aged4  
				 college     worker single      kids    pworker 
				 we ";

 oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1 & somecooking==1, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
*	outreg2 using "$res/mp_Fce_decomp", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");


* France
# delimit ;
local varexp "	age: aged1  aged2  aged3  aged4	
	, homemeals:  homem1 homem2 homem3 homem4" ;				

local varexp2 "  homem1 homem2 homem3 homem4 aged2  aged3  aged4  
				 college     worker single      kids    pworker 
				 we congel";

 oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0 & somecooking ==1, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(aged1 aged2 aged3 aged4) ;
*	outreg2 using "$res/mp_Fce_decomp", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");
set linesize 255
forvalues ct=1/4	{
	quietly reg sumcuisineprinc ib2.bndom single kids worker pworker we college i.aged	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc ib2.bndom single kids worker pworker we college i.aged	///
		congel lavevaisselle i.revclass[pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 meq1 meq2, not r2
	
forvalues ct=1/4	{
	quietly logit somecooking ib2.bndom single kids worker pworker we college i.aged	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly logit somecooking ib2.bndom single kids worker pworker we college i.aged	///
		congel lavevaisselle i.revclass[pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 meq1 meq2, not r2	
	
forvalues ct=1/4	{
	quietly reg sumcuisineprinc ib2.bndom single kids worker pworker we college i.aged	///
		[pw=poidsredressement2] if ct==`ct'  & somecooking>0, robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc ib2.bndom single kids worker pworker we college i.aged	///
		congel lavevaisselle i.revclass[pw=poidsredressement2] if ct==`ct' & somecooking>0, robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 meq1 meq2, not r2	
