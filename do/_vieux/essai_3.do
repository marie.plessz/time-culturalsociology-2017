
use  "$data/TMP_2_recode", clear


* pstatus 0: no partner 1 : partner no job; 2: partner works

*income reduced 
recode revclass (0 8000 10000=10000) (12000 14000 16000=16000)	///
	(18000 20000 24000=24000) (28000 32000 40000=40000) , gen(rev4cl)

/*	Le temps de cuisine a-t-il d�clin� � l'�chelle du m�nage?	*/	
******************************************************************
	
* temps cuisine et nb repas dom
table enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking mean bndom mean bnrepas)
table bndom country enquete [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking)
* tps cuisine et �quipement (Fce)
table enquete if country==0 [pw=poidsredressement2], c(mean lavevaisselle mean congel mean microonde)
corr lavevaisselle congel microonde if country==0
table enquete congel [pw=poidsredressement2] if country==0	///
	, c(mean microonde mean somecooking mean sumcuisineprinc mean bndom)

* tps cuisine et cposition m�nage
table enquete country [pw=poidsredressement2], c(mean single mean kids mean worker mean pworker)

table kids enquete country [pw=poidsredressement2], c(mean somecooking mean sumcuisineprinc mean bndom mean congel)


/*	Quelles sont les caract�ristiques des m�nages associ�es au d�clin dans les 2 pays?	*/
* toutes les variables sont cod�es de fa�on � faire appara�tre ce qui fait "plus cuisiner"

forvalues ct=1/4	{
	quietly reg sumcuisineprinc ib2.bndom i.pstatus kids nojob  we college i.aged	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc ib2.bndom i.pstatus kids nojob  we college i.aged	///
		congel lavevaisselle i.rev4cl [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 meq1 meq2, not r2
	
/*	D�composons les d�clins pour confirmer	*/	
	*	==> oaxaca
