* convergence entre Fce et US?

reg sumcuisineprinc i.country##i.enquete [pw=poidsredressement2] , robust

reg sumcuisineprinc i.country##(bndom0 bndom1 bndom3 bndom4  couple kids nojob	///
	college 	aged2 aged3 aged4 we)[pw=poidsredressement2] if enquete==1985, robust
reg sumcuisineprinc i.country##(bndom0 bndom1 bndom3 bndom4  couple kids nojob	///
	college 	aged2 aged3 aged4 we)[pw=poidsredressement2] if enquete==2009, robust	
	
reg sumcuisineprinc (bndom0 bndom1 bndom3 bndom4  couple kids nojob	///
	college 	aged2 aged3 aged4 we)##i_US##i_year2009  [pw=poidsredressement2] , robust	
	
reg sumcuisineprinc i_* we  [pw=poidsredressement2] , robust	
eststo m0

reg sumcuisineprinc i_* single kids nojob	///
	college 	aged2 aged3 aged4 we  [pw=poidsredressement2] , robust	
eststo m1

reg sumcuisineprinc i_* ib3.bndom single kids nojob	///
	college 	aged2 aged3 aged4 we  [pw=poidsredressement2] , robust	
eststo m3

esttab m0 m1 m3, not r2

reg sumcuisineprinc i_* i.bndom if single==1
eststo m2s
reg sumcuisineprinc i_* ib3.bndom kids nojob	///
	college 	aged2 aged3 aged4 we if single==0
eststo m2c

esttab m0 m1 m2s m2c, not r2



reg sumcuisineprinc i_2009  couple kids nojob	///
	college 	aged2 aged3 aged4 we if ct==4
eststo m3


reg sumcuisineprinc i_* i.bndom couple kids nojob	///
	college 	aged2 aged3 aged4 we if ct==4
eststo m4	


**************
*ANOVA
anova sumcuisineprinc i_* single kids nojob	///
	college i.aged  we  [pw=poidsredressement2] , robust	
anova sumcuisineprinc i_* single kids nojob	///
	college i.bndom	i.aged we  [pw=poidsredressement2] , robust	
anova sumcuisineprinc country##i.enquete##(single kids nojob	///
	college i.bndom	i.aged we )
* on a pas besoin de la triple interaction	
anova sumcuisineprinc country##(single kids nojob	///
	college i.bndom	i.aged we ) i.enquete##(single kids nojob	///
	college i.bndom	i.aged we ) i.enquete##country
