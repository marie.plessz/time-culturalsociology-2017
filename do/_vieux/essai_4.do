cap log close
set linesize 80
log using "$res/essai_4", text replace
set more off
use  "$data/TMP_2_recode", clear

/*	Le temps de cuisine a-t-il d�clin� � l'�chelle du m�nage?	*/	
******************************************************************

* temps cuisine et nb repas dom
table enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking mean bndom mean bnrepas)
table bndom enquete country [pw=poidsredressement2], c(mean sumcuisineprinc mean somecooking)
* tps cuisine et �quipement (Fce)
table enquete if country==0 [pw=poidsredressement2], c(mean lavevaisselle mean congel mean microonde)
corr lavevaisselle congel microonde if country==0
table enquete congel [pw=poidsredressement2] if country==0	///
	, c(mean microonde mean somecooking mean sumcuisineprinc mean bndom)

* tps cuisine et cposition m�nage
table enquete country [pw=poidsredressement2], c(mean single mean kids mean worker mean pworker)

table kids enquete country [pw=poidsredressement2], c(mean somecooking mean sumcuisineprinc mean bndom mean congel)


/*	Quelles sont les caract�ristiques des m�nages associ�es au d�clin dans les 2 pays?	*/
* toutes les variables sont cod�es de fa�on � faire appara�tre ce qui fait "plus cuisiner"
local varexp " bndom1 bndom2 bndom3 bndom4  pjob pnojob kids nojob college 	aged2 aged3 aged4 we"
local eq "congel lavevaisselle"
local inc "rev4cl1 rev4cl2 rev4cl3"

forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc `varexp' `eq' `inc' [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 , not r2 mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" )
	esttab meq1 meq2 , not r2 mtitle("Fce-85" "Fce-09")
log close
	
/*	D�composons les d�clins pour confirmer	*/	
	*	==> oaxaca
* USA
# delimit ;
local varexp "	partner : single pjob pnojob
	, age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4" ;				
local varexp2 " bndom1 bndom2 bndom3 bndom4  pjob pnojob kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4, single pjob pnojob, 
	bndom0 bndom1 bndom2 bndom3 bndom4" ;
quietly  oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_oaxaca", replace onecol noobs bdec(3) ctitle("USA")
	nose ;


* France	;
# delimit ;
local varexp "	partner : single pjob pnojob
	, age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4" ;				
local varexp2 " bndom1 bndom2 bndom3 bndom4  pjob pnojob kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4, single pjob pnojob, 
	bndom0 bndom1 bndom2 bndom3 bndom4" ;
quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_oaxaca", append onecol noobs bdec(3) ctitle("France") nose;


# delimit ;
local varexp "	partner : single pjob pnojob
	, age: aged1  aged2  aged3  aged4	
	, homemeals: bndom0 bndom1 bndom2 bndom3 bndom4
	, income: rev4cl1 rev4cl2 rev4cl3 rev4cl4 " ;				
local varexp2 " bndom1 bndom2 bndom3 bndom4 nojob pjob pnojob kids college 
	aged2 aged3 aged4 we congel lavevaisselle rev4cl1 rev4cl2 rev4cl3" ;

local cat "aged1 aged2 aged3 aged4, single pjob pnojob, 
	bndom0 bndom1 bndom2 bndom3 bndom4, rev4cl1 rev4cl2 rev4cl3 rev4cl4 " ;
quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail ( `varexp') categorical(`cat') ;
outreg2 using "$res/essai_oaxaca", append onecol noobs bdec(3) ctitle("France-full") nose;
