* survival


/* update: Fabrice me dit qu'on ne peut pas faire de mod�le de dur�e sur ces donn�es
car je n'ai pas de vraies dur�es, mais des sommes (relativement complexes) de dur�es 
*/
gen homem13=.
replace homem13=1 if nhomemeals==1
replace homem13=3 if nhomemeals==3

gen somecooking=sumcuisineprinc>0
replace somecooking=. if sumcuisineprinc==.

* en incluant les 0 (en ajoutant 0.1 � toutes les obs
gen stcuis=sumcuisineprinc+0.1
stset stcuis

sts
sts, by(enquete country)
sts, by(nhomemeals)

sts , by(homem13 enquete country )

stcox  enquete country
eststo m1
stcox  enquete country aged2  aged3  aged4  college worker single  kids    pworker
eststo m2
stcox  enquete country i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker
eststo m3

esttab m1 m2 m3 , wide not
***** mon mod�le pr�f�r� jusqu'ici : pond�r�, c
svy: stcox  i_* we
eststo m1
svy: stcox  i_* we aged2  aged3  aged4  college worker single  kids    pworker
eststo m2
svy: stcox  i_* we i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker
eststo m3

esttab m1 m2 m3 , wide not eform title(Hazard ratios)
*stcurve , survival at1(nhomemeals=3)  at2(nhomemeals=1) // autres variables � leur moyenne

svy: quietly stcox i.enquete##i.country we i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker
stcurve , survival at1(enquete=1985 country=0 nhomemeals=2 we=0)  ///
	at1(enquete=2009 country=0 nhomemeals=2 we=0)  /// autres variables � leur moyenne
	name(France, replace) order(1 "1985" 2 "2009") title(France)
stcurve , survival at1(enquete=1985 country=1 nhomemeals=2 we=0) ///
  at1(enquete=2009 country=1 nhomemeals=2 we=0)  /// autres variables � leur moyenne
  name(USA, replace) order(1 "1985" 2 "2009") title(USA)
 graph combine France USA
* sans les 0
stset sumcuisineprinc

sts , by(homem13 enquete country )


svy: stcox  i_* we
eststo m1
stcox  i_* we aged2  aged3  aged4  college worker single  kids    pworker
eststo m2
stcox  i_* we i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker
eststo m3

esttab m1 m2 m3 , wide not eform title(Hazard ratios)
stcurve , survival at1(nhomemeals=3)  at2(nhomemeals=1) // autres variables � leur moyenne

quietly stcox i.enquete##i.country i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker
stcurve , survival at1(enquete=1985 country=0 nhomemeals=2)   at1(enquete=2009 country=0 nhomemeals=2)  // autres variables � leur moyenne

*somecooking et nb de repas dom
logit somecooking i_* aged2  aged3  aged4  college worker single  kids    pworker
eststo s1
logit somecooking i_* aged2  aged3  aged4  college worker single  kids    pworker i.nhomemeals
eststo s2
esttab s1 s2, not wide eform title(Odds-ratios Some cooking)

svyset  [pweight=poidsredressement2]
svy: logit somecooking i_* aged2  aged3  aged4  college worker single  kids    pworker
eststo m1
logit somecooking i_* aged2  aged3  aged4  college worker single  kids    pworker [pweight=poidsredressement2]
eststo m2
esttab m1 m2, wide

svy:stcox  i_* i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker we
eststo m3
stcox  i_* i.nhomemeals aged2  aged3  aged4  college worker single  kids    pworker we
eststo m4
esttab m3 m4, wide eform
