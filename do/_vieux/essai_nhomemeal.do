set more off
# delimit ;

* France ;
 use "$path/mp_Fce_4_Estsample.dta", clear ;

 local varexpreg "aged2  aged3  aged4  
	 college     worker single      kids    pworker
					c.diffage10 urbain0 urbain1 urbain2 urbain3 " ;
local varexpreg2 "aged2  aged3  aged4  
	 college     worker single      kids    pworker
	c.diffage10 urbain0 urbain1 urbain2 urbain3 ib2.nhomemeals " ;	
	
 reg sumcuisineprinc i.enquete  `varexpreg'  [pweight=poidsredressement2]
	,robust;
eststo m1;
 reg sumcuisineprinc i.enquete  `varexpreg2'  [pweight=poidsredressement2]
	,robust;
	eststo m2;
reg nhomemeals i.enquete  `varexpreg'  [pweight=poidsredressement2]
	,robust;
eststo m3;
 	
*esttab m1 m2, not p wide;							 

* USA;
 use "$data/USAestsample_hld.dta", clear ;
local varexpreg "aged2  aged3  aged4  
					 college     worker single      kids    pworker
					c.diffage10  " ;
local varexpreg2 "aged2  aged3  aged4  
					 college     worker single      kids    pworker
					c.diffage10 ib2.nhomemeals " ;
					
 reg sumcuisineprinc i.enquete  `varexpreg'  [pweight=poidsredressement2]
	,robust;
eststo m1us;
 reg sumcuisineprinc i.enquete  `varexpreg2'  [pweight=poidsredressement2]
	,robust;
	eststo m2us;
reg nhomemeals i.enquete  `varexpreg'  [pweight=poidsredressement2]
	,robust;
eststo m3us;

esttab m1 m2 m3, not wide title(France) 
	mtitle("Cuisine" "Ac nmeals" "Nhomemeals");		
esttab m1us m2us m3us, not wide title(USA) 
	mtitle("Cuisine" "Ac nmeals" "Nhomemeals");		

 ****************************************
*	OAXACA du 18 juillet : syntaxe différente de 2013 je sais pas pourquoi
*j'ai du enlever l'option "relax"
***************************************** ;
 # delimit ;
 set more off ;
									
				
local varexpreg "  aged2  aged3  aged4  
					 college     worker single      kids    pworker
					c.diffage10 we homem0 homem1 homem3 homem4";
local varexp2 "  aged2  aged3  aged4  college worker single kids 
   pworker homem0 homem1 homem3 homem4
   we ";
local categ "aged1 aged2 aged3 aged4, homem0 homem1 homem2  homem3 homem4" ;
local detail "age: aged1 aged2 aged3 aged4
	, homemeals: homem0 homem1 homem2  homem3 homem4"	;
   
   local varsup " revclass2-revclass12  urbain0 urbain1 urbain2 urbain3  "	;
				 

di  "`allvardep'";
di  "`varexp'";
di  "`varexp2'";


*preserve;
*keep if we==0;
*keep if we==1;
local data1 " use "$path/mp_Fce_4_Estsample.dta", clear " ;
local data2 "  use "$data/USAestsample_hld.dta", clear " ;

# delimit ;	
	/*  1	Régression avec des interactions pour tenir compte des différences 
		d'effectifs entre les 2 enquêtes	*/

 reg sumcuisineprinc i.enquete##(`varexpreg') [pweight=poidsredressement2]
	, robust;
				 
	/* Mean Decomposition  1985-2009 */;
forvalues v= 1/2 { ;
`data`v''	;
	reg sumcuisineprinc `varexp2' if enquete==1985 [pweight=poidsredressement2], robust;
	outreg2 using "$res/essai_regress_pays`v'", replace bdec(3) ctitle("Mean-1985");
	
	reg sumcuisineprinc `varexp2' if enquete==2009 [pweight=poidsredressement2], robust;
	outreg2 using "$res/essai_regress_pays`v'", append bdec(3) ctitle("Mean-2009");
	
	quietly oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2], 
				by(old1985) weight(1) model1(reg, robust) model2(reg, robust) noisily /*relax*/
				detail(`detail') categorical(`categ') ;
	outreg2 using "$res/essai_decomp_pays`v'", replace onecol noobs bdec(3) ctitle("Mean 1985-2009");
} ;
