*append
/*
use  "$data/merge_Fce", clear
gen country=0
append using "$data/merge_US"
tab country, mis
replace country=1 if country==.
label var country "Country"
label def country 0 "0_France" 1 "0_USA", modify
label val country country

egen yp=group(country enquete)
gen i_year2009= enquete==2009
gen i_US= country==1
gen i_2009US=  enquete==2009 &  country==1
save "$data/TMP_5_merge", replace
*/

*r�gressions
set more off
# delimit ;
local varexpreg "aged2  aged3  aged4  college worker single  kids    pworker
					 " ;
/* comparer le lien entre cuisine et nb de repas � l'ext/� domicile/total : */ ;	
 reg sumcuisineprinc i_* we `varexpreg'  [pweight=poidsredressement2]
	,robust;
eststo m1;
reg sumcuisineprinc i_* we `varexpreg' ib2.bnext [pweight=poidsredressement2]
	,robust;
	eststo m2;
reg sumcuisineprinc i_* we `varexpreg' ib2.bnrepas [pweight=poidsredressement2]
	,robust;
eststo m3;
reg sumcuisineprinc i_* we `varexpreg' ib2.bndom [pweight=poidsredressement2]
	,robust;
eststo m4;


esttab m1 m2 m3 m4, not wide title(merge) r2 bic;		
/* conclusion : c'est le nb de repas � domicile qui est le plus pertinent 
(R2 max et BIC min)	
*/ ;
	
	exit;
reg bnext i_* we `varexpreg'  [pweight=poidsredressement2]
	,robust;
