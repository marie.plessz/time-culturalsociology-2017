
local distrib "country==1 & enquete==1985"
local coefs "country==1 & enquete==2009"

local varexp " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 	aged2 aged3 aged4 we"
local atmarg ""
foreach var of varlist `varexp' {
	quietly sum  `var'  if `distrib'
	*display `r(mean)'
	local m`var'  = `r(mean)' 
	*display "`m`var''"
	local m2`var'  `" `var' =`m`var'' "'
	*display "`m2`var''"
	local atmarg `" `atmarg' `m2`var'' "'
	*display "`atmarg'"
	}
	
	*****
display "`atmarg'"

regress sumcuisineprinc `varexp' if `coefs'
margins, at ("`atmarg'")  
margins



***********************
***	 ou bcp plus simple : 
regress sumcuisineprinc single aged1-aged4 if country==1
margins if country==0, noesample
/* pas besoin de pond�rer le margins si la r�gression l'est	*/
/* quels seraient les temps de cuisine en 2009 si les populations n'avaient pas chang�? */
tab ct

local varexp " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 	aged2 aged3 aged4 we"

quietly reg sumcuisineprinc `varexp' 	[pw=poidsredressement2] if ct==1 , robust
*France
margins
margins if ct==2 , noesample
quietly reg sumcuisineprinc `varexp' 	[pw=poidsredressement2] if ct==3 , robust
*USA
margins
margins if ct==4, noesample

quietly reg sumcuisineprinc `varexp' 	[pw=poidsredressement2] if ct==1 , robust
*1985
margins
margins if ct==3, noesample
quietly reg sumcuisineprinc `varexp' 	[pw=poidsredressement2] if ct==2 , robust
*2009
margins
margins if ct==4, noesample
	