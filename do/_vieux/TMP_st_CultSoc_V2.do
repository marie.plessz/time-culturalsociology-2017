cap log close
set linesize 85
log using "$res/TMP_st_CultSoc_V2", text replace
set more off
use  "$data/TMP_2_recode", clear

/* /!\ le package oaxaca a �t� mis � jour. j'utilise ici l'ancienne version.
sur un ordi o� mise � jour a �t� faite (adoupdate) remplacer oaxaca par oaxaca9.
dans ma version l'option pour g�rer les vars cat�gorielles s'appelle categorical
dans la nouvelle elle s'appelle normalize et s'utilise diff�remment.
*/

* seult pr�sence ou non du conjoint
******TABLE 1
/*	Le temps de cuisine a-t-il d�clin� � l'�chelle du m�nage?	*/	
******************************************************************
svyset [pw=poidsredressement2]
* temps cuisine et nb repas dom
table enquete country [pw=poidsredressement2], c(mean somecooking )  format (%6.2f)
table enquete country [pw=poidsredressement2] if somecooking==1, c(mean sumcuisineprinc )  format (%6.2f)

table enquete country [pw=poidsredressement2], c(mean sumcuisineprinc)  format (%6.2f)
table enquete  [pw=poidsredressement2] if country==0, c(mean lavevaisselle mean congel)  format (%6.2f)

table enquete country [pw=poidsredressement2], c(mean bndom mean bnext)  format (%6.2f)

svyset [pw=poidsredressement2]
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean sumcuisineprinc ) sum replace f(0) clab(sumcuisineprinc _ _) ///
	layout(rb) h3(Household cooking time)  ptotal(none) h1(CultSoc: Descriptive statistics)   
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean somecooking) sum append f(1p) h1(nil) h2(nil) clab(_ _ _) ///
	layout(rb) h3(somecooking) ptotal(none) 
tabout enquete country if somecooking==1 using "$res/CultSoc_desc1.csv" , svy c(mean sumcuisineprinc) sum append f(0) h1(nil) h2(nil) clab(_ _ _) ///
	layout(rb) h3(CondCooking)  ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean lavevaisselle) sum append f(1p) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Dishwasher) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean congel) sum append f(1p) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Freezer) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean bndom) sum append f(1) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Number of eating events at home (main cook)) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean bnext) sum append f(1) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Number of eating events away from home (main cook)) ptotal(none) 

	
******* TABLE 2	
	
tabout  aged rev4cl ct using "$res/CultSoc_desc2.csv",  replace f(1p)   c(col) style(csv)
	
quietly svy : regress bndom ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(1) noaster nor2 noobs nodepvar
quietly svy : regress bnext ibn.ct, nocons	 
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(1) noaster nor2 noobs nodepvar
quietly svy : regress couple ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress kids ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress nojob ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress college ibn.ct, nocons
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar	
quietly svy : regress lavevaisselle ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster nor2 noobs nodepvar
quietly svy : regress congel ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
	

************* TABLE 3
/*	Quelles sont les caract�ristiques des m�nages associ�es au d�clin dans les 2 pays?	*/
* toutes les variables sont cod�es de fa�on � faire appara�tre ce qui fait "plus cuisiner"
local varexp " bndom1 bndom2 bndom3 bndom4  couple kids nojob college 	aged2 aged3 aged4 we"
local eq "congel lavevaisselle"
local inc "rev4cl1 rev4cl2 rev4cl3"
cap drop predct*
forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	predict  predct`ct'	// pr�diction pour tous les individus ac les 4 r�gressions
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc `varexp' `eq' `inc' [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 , not r2  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: comparaison)
	esttab meq1 meq2 , not r2  mtitle("Fce-85" "Fce-09") 	///
		title(Temps de cuisine: France full model )
	esttab  m3 m4 m1 m2 meq1 meq2 , not r2  mtitle("USA-85" "USA-09" "Fce-85" "Fce-09" "Fce-85" "Fce-09" ) ///
		title(Temps de cuisine: comparaison) 
	 
forvalues ct=1/4	{
	quietly reg cuisineprinc `varexp' 	///
		[pw=poidsredressement2] if ct==`ct' & sexe==0, robust
	eststo mck`ct'
	}	
esttab mck1 mck2 mck3 mck4 , not r2 p  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: femmes )
cap log close


******* TABLE 4
					
	*	==> oaxaca

* pr�paration des macros contenant les variables
# delimit ;
local varexp "	age:  aged1 aged2  aged3  aged4	
	, homemeals:  bndom0 bndom1 bndom2 bndom3 bndom4, couple: couple single
	, kids:  kids nokids, worker: worker nojob, educ:college nocoll, we: we nowe" ;				
local varexp2 " bndom0 bndom1 bndom3 bndom4  couple kids nojob college 
	aged2 aged3 aged4 we";
local cat "aged1 aged2 aged3 aged4 
	, bndom0 bndom1 bndom2 bndom3 bndom4
	, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;
local det " Variables: aged1 aged2 aged3 aged4 bndom0 bndom1 bndom2 bndom3 bndom4
	single couple kids nokids worker nojob college nocoll we nowe
	" ;
	
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') detail(`det') ;
outreg2 using "$res/`tag'_oaxaca", replace  noobs bdec(2) ctitle("USA") 
	side noparen onecol noaster /* pour pouvoir calculer les ci */;				 
matrix K= e(b)	/* stocke les r�sultats		*/	 ;

*France ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) model1(reg, robust) model2(reg, robust)
				 categorical(`cat') detail(`det') ;	
outreg2 using "$res/`tag'_oaxaca", append  noobs bdec(2) ctitle("France") 
	side noparen onecol noaster /* pour pouvoir calculer les ci */;				 
matrix K=(K \ e(b)) ;					 
matrix list K ;
matrix R=K' ;
matrix colnames R =USA France;
matrix list R,  format(%6.1f); /* tr�s bien mais ne contient pas les IC */

**** RESULTATS CPOMPLEMENTAIRES

*France complet  avec revenu et lave vaisselle ;

oaxaca sumcuisineprinc `varexp2'   lavevaisselle rev4cl1 rev4cl2 rev4cl3 
			[pweight=poidsredressement2] if country==0, 
				by(old1985) model1(reg, robust) model2(reg, robust)
				 categorical(`cat' , nolave lavevaisselle, rev4cl1 rev4cl2 rev4cl3 rev4cl4) ;	
matrix K=(K \ e(b)) ;					 
matrix list K ;

* 2009 comparaison Fce-USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if enquete==2009, 
				by(country) model1(reg, robust) model2(reg, robust) 
				categorical(`cat') ;

	nose ;				
matrix K=(K \ e(b)) ;					 
matrix list K ;


* test si on swape cad si on inverse les ann�es de r�f�rence: 		 		
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				categorical(`cat') swap ;	
matrix K=(K \ e(b)) ;					 
matrix list K ;				
*France ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985) model1(reg, robust) model2(reg, robust)
				 categorical(`cat') swap ;	
matrix K=(K \ e(b)) ;					 
matrix list K ;				 
* 2009 ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if enquete==2009, 
				by(country) model1(reg, robust) model2(reg, robust) 
				categorical(`cat') swap ;
matrix K=(K \ e(b)) ;					 
matrix list K ;
#delimit ;
matrix rownames K = USA Fce 2009	USA Fce 2009	;	
matrix roweq K = basic basic basic swap swap swap ;	
matrix list K , format(%6.3f) ;
exit;				

				 
**** verif sur les femmes seulement	TABLE S1			 ;
# delimit cr
gen parfem=cuisineprinc/sumcuisineprinc
regress parfem ibn.ct if sexe==0 & couple==1, nocons

tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean sumcuisineprinc ) sum append f(0) clab(sumcuisineprinc _ _) ///
	layout(rb) h3(Household cooking time)  ptotal(none) h1(CultSoc: Descriptive statistics)   
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean parfem ) sum append f(2) clab(sumcuisineprinc _ _) ///
	layout(rb) h3(Household cooking time)  ptotal(none) h1(CultSoc: Descriptive statistics) 
				 
exit;

* non utilis� : calcul des pr�dictions

/* utilisons les pr�dictions */
/*
            tabulation:  Freq.   Numeric  Label
                          6500         1  Fce-85
                         11417         2  Fce-09
                           811         3  USA-85
                          7068         4  USA-09
*/
* France: prediction for 2009
	* observed in 1985
 mean predct1 if ct==1 [pw=poidsredressement2]
	*if associations had not changed
 mean predct1 if ct==2 [pw=poidsredressement2] 
	*if population had not changed
 mean predct2 if ct==1 [pw=poidsredressement2] 	
	* observed in 2009
 mean predct2 if ct==2 [pw=poidsredressement2] 	

 * US: prediction for 2009 
	* observed in 1985
 mean predct3 if ct==3 [pw=poidsredressement2]
	*if associations had not changed
 mean predct3 if ct==4 [pw=poidsredressement2] 
	*if population had not changed
 mean predct4 if ct==3 [pw=poidsredressement2] 	
	* observed in 2009
 mean predct4 if ct==4 [pw=poidsredressement2] 		
/*	D�composons les d�clins pour confirmer	*/	
*	NB :   decomposition threefold ;
