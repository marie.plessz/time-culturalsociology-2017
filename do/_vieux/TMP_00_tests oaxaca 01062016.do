/*
Marie plessz le 01/06/2016
projet TIME
essais sur les diverses options de la commande oaxaca. la d�composition est tr�s
sensible � la fa�on dont elle est sp�cifi�e, qui est largement arbitraire.
je simplifie mon mod�le on gardant seulement la var bndom, cat�gorielle.
*/

/*
* g�n�rer les donn�es
use  "$data/TMP_2_recode", clear
gen random=runiform()
sort ct random
bys ct: gen insample= _n<600
tab insample ct
keep if insample==1
keep insample hldid persid ct country sumcuisineprinc bndom* 	///
	poidsredressement2 old1985
label data "Time use surveys pour test Oaxaca"
note _dta: extrait des enqu�tes Emploi du temps France (85 et 2009) et USA ///
	(85 et 2009) pour tester la commande  Oaxaca facilement. Marie Plessz ///	
	le 01/06/2016.
save TIME_testoaxaca, replace
*/

* d�but
cd XXXX // remplacer par l'emplacement du dossier.
use TIME_testoaxaca, clear
set more off
# delimit ;


	*#1: effet de weight ;
	/* weight commande quelle ann�e est prises comme r�f pour calculer les effets
	unexplained/explained, dans le cas d'une d�composition en 2 et non en 3 */
*weight(0) ;	
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4 [pweight=poidsredressement2] 
	if country==1 , by(old1985) weight(0)  model1(reg, robust) model2(reg, robust)
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4)  ;	
*weight(1) ;					 
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4[pweight=poidsredressement2] 
	if country==1 , by(old1985) weight(1) model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4)  ;	
*weight(0.5) ;					 
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4[pweight=poidsredressement2] 
	if country==1, by(old1985) weight(0.5) model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4)  ;	
*d�composition threefold	;			 
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4[pweight=poidsredressement2] 
	if country==1, by(old1985)  model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4)  ;	
/* ==> weight r�partit la partie "interaction" de threefold dans
 l'expleined ou l'unexplained, selon la valeur de weight. la constante est 
 toujours dans l'unexplained car techniquement c un coefficient.
 Threefold est rassurant car + stable, mais la partie "interaction" est tr�s 
 compliqu�e � interpr�ter */ 
 
	*#2: ann�e de r�f�rence ;
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4[pweight=poidsredressement2] 
if country==1, 	by(old1985)  model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4)  ;
*option swap inverse les groupes		;		 
oaxaca sumcuisineprinc bndom0 bndom1 bndom3 bndom4[pweight=poidsredressement2] 
	if country==1, 	by(old1985)  model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4) swap ;	
/* ==> la r�partition entre coef et endowments change, car les diff�rences sont 
pas pond�r�es par la m�me ann�e. ici j'ai fait avec threefold, mais avec 
weight c'est pareil la r�partion change car ce qui est en jeu c'est l'ann�e
de r�f�rence pour calculer le "volume total" de la diff�rence entre X et de la 
diff�rence entre coefs.	*/	

				*#3: var categorielles ;
				# delimit ;
* avec option categorical()		;		
oaxaca sumcuisineprinc bndom0 bndom1  bndom3 bndom4 [pweight=poidsredressement2] 
	if country==1, 		by(old1985)  model1(reg, robust) model2(reg, robust) 
				 categorical(bndom0 bndom1 bndom2 bndom3 bndom4) detail  ;
* sans option categorical();
oaxaca sumcuisineprinc bndom0 bndom1  bndom3 bndom4 [pweight=poidsredressement2] 
	if country==1,  	by(old1985)  model1(reg, robust) model2(reg, robust) 
	detail   ;
* sans option categorical() et en changeant cag� de r�f;
oaxaca sumcuisineprinc bndom2 bndom1  bndom3 bndom4 [pweight=poidsredressement2]
	if country==1, 	by(old1985)  model1(reg, robust) model2(reg, robust) detail ;
/*	==> la d�composition globale ne change pas (que l'on utilise weight ou 
threefold). Par contre la d�composition d�taill�e change car la 
constante varie selon la cat� de r�f, donc la "part attribuable � une var" varie
*/	
