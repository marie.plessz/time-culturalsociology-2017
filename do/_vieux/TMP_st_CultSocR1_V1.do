cap log close
set linesize 85
log using "$res/TMP_st_CultSocR1_V1", text replace
set more off
use  "$data/TMP_2_recode", clear

/* R1 : isoler effet de bndom ds les résultats ET traiter bndom comme une var continue... 
toutefois j'ai traité comme continue une var bornée à 4 (150 cas >4 aux USA en 
cumulant les 2 enquetes).


/* /!\ le package oaxaca a été mis à jour. j'utilise ici l'ancienne version.
sur un ordi où mise à jour a été faite (adoupdate) remplacer oaxaca par oaxaca9.
dans ma version l'option pour gérer les vars catégorielles s'appelle categorical
dans la nouvelle elle s'appelle normalize et s'utilise différemment.
*/

* seult présence ou non du conjoint
******TABLE 1
/*	Le temps de cuisine a-t-il décliné à l'échelle du ménage?	*/	
******************************************************************
svyset [pw=poidsredressement2]
* temps cuisine et nb repas dom
table enquete country [pw=poidsredressement2], c(mean somecooking )  format (%6.2f)
table enquete country [pw=poidsredressement2] if somecooking==1, c(mean sumcuisineprinc )  format (%6.2f)

table enquete country [pw=poidsredressement2], c(mean sumcuisineprinc)  format (%6.2f)
table enquete  [pw=poidsredressement2] if country==0, c(mean lavevaisselle mean congel)  format (%6.2f)

table enquete country [pw=poidsredressement2], c(mean bndom mean bnext)  format (%6.2f)

svyset [pw=poidsredressement2]
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean sumcuisineprinc ) sum replace f(0) clab(sumcuisineprinc _ _) ///
	layout(rb) h3(Household cooking time)  ptotal(none) h1(CultSoc: Descriptive statistics)   
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean somecooking) sum append f(1p) h1(nil) h2(nil) clab(_ _ _) ///
	layout(rb) h3(somecooking) ptotal(none) 
tabout enquete country if somecooking==1 using "$res/CultSoc_desc1.csv" , svy c(mean sumcuisineprinc) sum append f(0) h1(nil) h2(nil) clab(_ _ _) ///
	layout(rb) h3(CondCooking)  ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean lavevaisselle) sum append f(1p) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Dishwasher) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean congel) sum append f(1p) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Freezer) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean bndom) sum append f(1) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Number of eating events at home (main cook)) ptotal(none) 
tabout enquete country using "$res/CultSoc_desc1.csv", svy c(mean bnext) sum append f(1) h1(nil) h2(nil)  clab(_ _ _) ///
	layout(rb) h3(Number of eating events away from home (main cook)) ptotal(none) 

	
******* TABLE 2	
	
tabout  aged rev4cl ct using "$res/CultSoc_desc2.csv",  replace f(1p)   c(col) style(csv)
	
quietly svy : regress bndom ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(1) noaster nor2 noobs nodepvar
quietly svy : regress bnext ibn.ct, nocons	 
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(1) noaster nor2 noobs nodepvar
quietly svy : regress couple ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress kids ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress nojob ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
quietly svy : regress college ibn.ct, nocons
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar	
quietly svy : regress lavevaisselle ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster nor2 noobs nodepvar
quietly svy : regress congel ibn.ct, nocons	
	outreg2 using "$res/CultSoc_desc_outreg",  stats(coef)  dec(3) noaster	 nor2 noobs nodepvar
	

************* TABLE 3
/*	Quelles sont les caractéristiques des ménages associées au déclin dans les 2 pays?	*/
* toutes les variables sont codées de façon à faire apparaître ce qui fait "plus cuisiner"
local varexp " bndom  couple kids nojob college 	aged2 aged3 aged4 we"
local eq "congel lavevaisselle"
local inc "rev4cl1 rev4cl2 rev4cl3"
cap drop predct*
forvalues ct=1/4	{
	quietly reg sumcuisineprinc `varexp'	///
		[pw=poidsredressement2] if ct==`ct', robust
	eststo m`ct'
	predict  predct`ct'	// prédiction pour tous les individus ac les 4 régressions
	}
forvalues ct=1/2	{
	quietly reg sumcuisineprinc `varexp' `eq' `inc' [pw=poidsredressement2] if ct==`ct', robust
	eststo meq`ct'
	}
	esttab m1 m2 m3 m4 , not r2  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: comparaison)
	esttab meq1 meq2 , not r2  mtitle("Fce-85" "Fce-09") 	///
		title(Temps de cuisine: France full model )
	esttab  m3 m4 m1 m2 meq1 meq2 , not r2  mtitle("USA-85" "USA-09" "Fce-85" "Fce-09" "Fce-85" "Fce-09" ) ///
		title(Temps de cuisine: comparaison) 
	 
forvalues ct=1/4	{
	quietly reg cuisineprinc `varexp' 	///
		[pw=poidsredressement2] if ct==`ct' & sexe==0, robust
	eststo mck`ct'
	}	
esttab mck1 mck2 mck3 mck4 , not r2 p  mtitle("Fce-85" "Fce-09" "USA-85" "USA-09" ) ///
		title(Temps de cuisine: femmes )
cap log close


	# delimit ;
local varexp2 " bndom  (controls: single couple nokids kids worker nojob college nocoll
	aged1 aged2 aged3 aged4 we nowe)";	
local cat "aged1 aged2 aged3 aged4 
		, single couple, kids nokids, worker nojob, college nocoll, we nowe" ;	
	
*USA ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==1, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/`tag'_oaxaca", replace  noobs bdec(2) ctitle("USA") 
	side noparen onecol  nose /* pour pouvoir calculer les ci */;				 
	
*Fce ;
oaxaca sumcuisineprinc `varexp2' [pweight=poidsredressement2] if country==0, 
				by(old1985)  model1(reg, robust) model2(reg, robust)
				 categorical(`cat') noisily ;
outreg2 using "$res/`tag'_oaxaca", append  noobs bdec(2) ctitle("France") 
	side noparen onecol  nose /* pour pouvoir calculer les ci */;		
